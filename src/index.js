// JS
import './js/'

// SCSS
import "bootstrap/dist/css/bootstrap-grid.css"
import './assets/scss/common.scss'
// import "swiper/css/swiper.min.css"
// import 'swiper/swiper-bundle.css';
import 'animate.css'

// Vue.js
/*window.Vue = require('vue')

// Vue components (for use in html)
Vue.component('example-component', require('./components/Example.vue').default)

// Vue init
const app = new Vue({
  el: '#app'
})
 */

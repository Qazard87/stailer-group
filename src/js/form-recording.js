import IMask from 'imask'

document.addEventListener("DOMContentLoaded", function () {

    let form = document.querySelector("#partners-form-blank");

    if (!form) return;

    //маска номера телефона
    let phoneMask = IMask(
        form.querySelector('#phone-mask'), {
            mask: '+{7}(000)000-00-00'
        }),
        btnSubmit = form.querySelector("#submit-partners-blank"),
        address = form.querySelectorAll(".js-address"),
        count = 0,
        agreeTerms = form.querySelector("#agree_terms");

    //Обработчики событий
    agreeTerms.addEventListener("change", checkAgreeTerms);
    form.addEventListener('submit', fetchForm);
    for (let i = 0; i < form.querySelectorAll("input").length; i++) {
        let input = form.querySelectorAll("input")[i];
        input.addEventListener("input", removeSingleErrorNode)
    }
    form.querySelector(".js-add-address").addEventListener("click", addNewAddress)

    //Функции обработчиков

    function validateForm() {

        removeErrorNodes();

        let inputs = form.querySelectorAll("input[type='text']"),
            email = form.querySelectorAll("input[name='email']"),
            shopNumber = form.querySelectorAll("input[type='number']"),
            violations = 0;

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].name !== "email") {
                if (inputs[i].value.trim() === "") {
                    createNoticeNode(inputs[i], "text-error", "Пожалуйста, заполните поле");
                    violations++;
                }
            }
        }

        for (let i = 0; i < email.length; i++) {
            if (email[i].value.trim() === "") {
                createNoticeNode(email[i], "text-error", "Пожалуйста, заполните поле");
                violations++;
                continue;
            }
            let regExp = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/

            if (email[i].value.match(regExp) === null) {
                createNoticeNode(email[i], "text-error", "Некорректный email");
                violations++;
            }
        }

        for (let i = 0; i < shopNumber.length; i++) {

            let regExp = /\d+/;

            if (shopNumber[i].value.match(regExp) === null) {
                createNoticeNode(shopNumber[i], "text-error", "Некорректное число магазинов");
                violations++;
                continue;
            }

            if (shopNumber[i].value <= 0) {
                createNoticeNode(shopNumber[i], "text-error", "Значение поля должно быть положительным");
                violations++;
            }
        }

        if (!agreeTerms.checked) {
            createNoticeNode(agreeTerms, "text-error", "Поле обязательно для заполнения");
            violations++;
        }

        return violations;
    }

    function fetchForm(e) {
        e.preventDefault();

        let violations = validateForm();

        form.style.maxHeight = form.scrollHeight + "px";

        if (violations > 0) {
            scrollToFirstError();
            return;
        }

        toggleSubmitBtn();

        let url = "/assets/data/message-success.json",
            data = new FormData(form),
            request = new XMLHttpRequest();

        request.responseType = "json";
        request.open("post", url, true);
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');

        request.addEventListener("readystatechange", function (e) {

            if (request.readyState === 4 && request.status === 200) {

                if (this.response.err === 0 && this.response.elements.length === 1) {
                    createSuccessMovements(btnSubmit, this.response.elements[0].message);
                    form.style.maxHeight = form.scrollHeight + "px";
                    return;
                }

                for (let i = 0; i < this.response.elements.length; i++) {

                    let message = this.response.elements[i].message,
                        errName = this.response.elements[i].errName;

                    for (let j = 0; j < form.elements.length; j++) {
                        let current = form.elements[j];

                        if (errName === current.name) {
                            createNoticeNode(current, "text-error", message);
                        }
                    }
                }
                scrollToFirstError();
            }
            toggleSubmitBtn();
        });

        request.send(data);
    }

    function scrollToFirstError() {
        let firstErr = document.querySelector(".border-error");

        if (!firstErr) return;

        let top = firstErr.getBoundingClientRect().top + pageYOffset,
            windowTop = document.body.scrollTop || window.pageYOffset,
            timer;

        if (top < windowTop) {
            timer = setInterval(function () {
                if (windowTop > top) window.scroll(0, windowTop -= 20); else clearInterval(timer)
            }, 5);
        } else {
            timer = setInterval(function () {
                if (windowTop < top) window.scroll(0, windowTop += 20); else clearInterval(timer)
            }, 5);
        }
    }

    function createNoticeNode(current, cls, message) {
        let p = document.createElement("p");
        p.classList.add(cls);
        if (cls === "text-error") {
            current.classList.add("border-error")
        }
        p.innerHTML = message;
        current.after(p);
    }

    function createSuccessMovements(current, message) {
        createNoticeNode(current, "text-success", message)
        btnSubmit.value = btnSubmit.dataset.wait;
    }

    function removeErrorNodes() {
        let inputs = form.querySelectorAll("input"),
            p = form.querySelectorAll(".text-error");

        if (!p.length) return;

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].classList.contains("border-error")) {
                inputs[i].classList.remove("border-error");
                inputs[i].nextElementSibling.remove();
            }
        }
    }

    function removeSingleErrorNode() {

        if (this.type === "email") {
            this.removeAttribute("invalid");
        }

        let isErrorNode = this.classList.contains("border-error");

        if (isErrorNode) {
            this.classList.remove("border-error");
            this.nextElementSibling.remove();
        }
    }

    function toggleSubmitBtn() {
        btnSubmit.disabled = !btnSubmit.disabled;

        let newValue = btnSubmit.dataset.wait;
        btnSubmit.dataset.wait = btnSubmit.value;
        btnSubmit.value = newValue;
    }

    function checkAgreeTerms() {
        let label = this.parentNode.querySelector("label.form-block__label");

        if (this.checked) {
            label.classList.add("checked");
        } else label.classList.remove("checked");
    }

    function addNewAddress() {
        count++;
        let clone = this.previousElementSibling.cloneNode(true);

        if (clone.querySelector("input").classList.contains("border-error")) {
            clone.querySelector("input").classList.remove("border-error")
        }
        if (clone.querySelector(".text-error")) {
            clone.querySelector(".text-error").remove();
        }
        let nameAttr = "address-" + count;
        clone.querySelector("input").name = nameAttr;
        clone.querySelector("input").value = "";
        this.previousElementSibling.after(clone);
        addListenersForNodes();
        form.style.maxHeight = form.scrollHeight + "px";
    }

    function addListenersForNodes() {
        let addresses = form.querySelectorAll(".js-address input");

        for(let i = 0; i<addresses.length; i++){
            addresses[i].addEventListener("input", removeSingleErrorNode);
        }
    }
});

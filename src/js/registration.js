"use strict";
import IMask from 'imask';

document.addEventListener("DOMContentLoaded", function () {
    let formRegister = document.querySelector("#registration");

    if (!formRegister) return;

    let mask = new IMask(
        formRegister.querySelector('input[type=\"phone\"]'), {
            mask: '+{0}(000)000-00-00'
        }
    )

    let agreeTerms = formRegister.querySelector("#agree_terms_checkbox"),
        newsTerms = formRegister.querySelector("#agree_news"),
        inputs = formRegister.querySelectorAll("input[type=\"text\"]"),
        email = formRegister.querySelector("#regEmail"),
        phone = formRegister.querySelector("input[type=\"phone\"]"),
        selects = formRegister.querySelectorAll("select"),
        btnSubmit = formRegister.querySelector("#regBtn"),
        violations = 0;


    //Event listeners
    agreeTerms.addEventListener("change", checkAgreeTerms);
    newsTerms.addEventListener("change", checkNewsTerms);
    phone.addEventListener("input", removeSingleErrorNode);
    formRegister.addEventListener("submit", fetchForm);
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("input", removeSingleErrorNode)
    }
    for (let i = 0; i < selects.length; i++) {
        selects[i].addEventListener("change", removeSingleErrorNode)
    }

    //Listeners functions
    function checkAgreeTerms() {
        let label = this.parentNode.querySelector(".form__label");

        if (this.classList.contains("border-error")) {
            this.classList.remove("border-error");
            this.nextElementSibling.remove();
        }

        if (this.checked) {
            label.classList.add("checked");
        } else label.classList.remove("checked");

    }

    function checkNewsTerms() {
        let label = this.parentNode.querySelector(".form__label");

        if (this.classList.contains("border-error")) {
            this.classList.remove("border-error");
            this.nextElementSibling.remove();
        }

        if (this.checked) {
            label.classList.add("checked");
        } else label.classList.remove("checked");
    }

    function createErrorNode(current, message) {
        let p = document.createElement("p");
        p.classList.add("text-error");
        current.classList.add("border-error")
        p.innerHTML = message;
        current.after(p);
    }

    function removeSingleErrorNode() {
        let isErrorNode = this.classList.contains("border-error");

        if (isErrorNode) {
            this.classList.remove("border-error");
            this.nextElementSibling.remove();
        }
    }

    function removeAllErrors() {
        let inputs = formRegister.querySelectorAll("input"),
            selects = formRegister.querySelectorAll("select"),
            p = formRegister.querySelectorAll(".text-error");

        if (!p.length) return;

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].classList.contains("border-error")) {
                inputs[i].classList.remove("border-error");
                inputs[i].nextElementSibling.remove();
            }
        }

        for (let i = 0; i < selects.length; i++) {
            if (selects[i].classList.contains("border-error")) {
                selects[i].classList.remove("border-error");
                selects[i].nextElementSibling.remove();
            }
        }
    }

    function checkAgreeSelection() {
        if (!agreeTerms.checked) {
            createErrorNode(agreeTerms, "Поле обязательно для заполнения")
            violations++;
        }
    }

    function validateInputs() {
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].name === "email") continue;
            if (!inputs[i].value) {
                createErrorNode(inputs[i], "Поле обязательно для заполнения")
                violations++;
            }
        }
    }

    function validateSelects() {
        for (let i = 0; i < selects.length; i++) {
            if (!selects[i].value) {
                createErrorNode(selects[i], "Поле обязательно для заполнения")
                violations++;
            }
        }
    }

    function validatePhone() {
        if (!phone.value) {
            createErrorNode(phone, "Поле обязательно для заполнения")
            violations++;
            return;
        }

        if (phone.value.length !== 16) {
            createErrorNode(phone, "Некорректный номер телефона");
            violations++;
            return;
        }

    }

    function validateEmail() {
        let regExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

        if (!email.value) {
            createErrorNode(email, "Поле обязательно для заполнения")
            violations++;
            return;
        }

        if (email.value.match(regExp) === null) {
            createErrorNode(email, "Некорректный email");
            violations++;
            return;
        }

    }

    function validateForm() {
        violations = 0;
        checkAgreeSelection();
        validateInputs();
        validateEmail();
        validatePhone();
        validateSelects();
    }

    function toggleSubmitBtnDisable() {
        btnSubmit.disabled = !btnSubmit.disabled;

        let newLabel = btnSubmit.dataset.wait;
        btnSubmit.dataset.wait = btnSubmit.value;
        btnSubmit.value = newLabel;
    }

    function fetchForm(e) {
        e.preventDefault();
        removeAllErrors();
        validateForm();

        if (violations > 0) return;

        toggleSubmitBtnDisable();
        fetchData();
    }

    function fetchData() {
        let url = "/assets/data/reg-success.json",
            data = new FormData(formRegister)

        let response = fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: data
        })
            .then(response => response.json())
            .then(result => {
                if (result.err === 0) {
                    let modals = document.querySelectorAll(".modal"),
                        modalSuccess = document.querySelector(".modal-status");
                    for (let i = 0; i < modals.length; i++) {
                        if (modals[i].classList.contains("show")) {
                            modals[i].classList.remove("show");
                        }
                    }
                    modalSuccess.classList.add("show");
                    modalSuccess.querySelector(".modal__title").innerHTML = result.elements[0].message
                    modalSuccess.querySelector("p.modal__info").innerHTML = result.elements[1].message
                    toggleSubmitBtnDisable();
                    return;
                } else {
                    for (let i = 0; i < result.elements.length; i++) {

                        let message = result.elements[i].message,
                            errName = result.elements[i].errName;

                        for (let j = 0; j < formRegister.elements.length; j++) {
                            let current = formRegister.elements[j];

                            if (errName === current.name) {
                                createErrorNode(current, message);
                            }
                        }
                    }
                    toggleSubmitBtnDisable();
                }
            })
    }
})
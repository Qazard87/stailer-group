"use strict";

document.addEventListener("DOMContentLoaded", function () {
    let accordeon = document.querySelector(".accordeon");

    if(!accordeon) return;

    var btn = accordeon.querySelectorAll(".accordeon__button");

    for (var i = 0; i < btn.length; i++) {
        var curr = btn[i];

        curr.addEventListener("click", function () {
            this.classList.toggle("active");

            var isActive = this.classList.contains("active");
            if (!isActive) {
                this.nextElementSibling.style.maxHeight = null;
            }else{
                this.nextElementSibling.style.maxHeight = this.nextElementSibling.scrollHeight + "px";
            }
        })
    }
})
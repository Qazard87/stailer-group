"use strict";

// import Swiper from "swiper";

//scroll to top
document.addEventListener("DOMContentLoaded", function () {
    let toTop = document.querySelector(".btn-to-top"),
        clientHeight = document.documentElement.clientHeight,
        offset = null;

    window.addEventListener("scroll", findYOffset);
    toTop.addEventListener("click", scrollToTop);

    function findYOffset() {
        offset = window.pageYOffset;

        if (offset >= clientHeight) {
            toTop.style.display = "block";
        } else {
            toTop.style.display = "";
        }
    }

    function scrollToTop() {
        let windowTop, timer;
        windowTop = document.body.scrollTop || window.pageYOffset;
        timer = setInterval(function () {
            if (windowTop > 0) window.scroll(0, windowTop -= 45); else clearInterval(timer)
        }, 5);
    }
})

//clearfix menu to filters
document.addEventListener("DOMContentLoaded", function () {
    let block = document.querySelector(".js-height-clearfix"),
        picture = document.querySelector(".js-height-image");

    if (!picture) return;

    window.addEventListener("resize", calcBlockHeight);

    calcBlockHeight();

    function calcBlockHeight() {
        let height = picture.getBoundingClientRect().height,
            padding = 15;

        if (screen.width > 991) {
            block.style.height = height + padding + "px";
        } else block.style.height = 0 + "px";
    }
})

//toggle card item width
document.addEventListener("DOMContentLoaded", function (e) {
    let btnSize = document.querySelector(".filters__menu");
    let blocks = document.querySelectorAll(".sep");
    let breakpoint = 767;

    if (!btnSize || !blocks) return;

    btnSize.addEventListener("click", toggleCardSize);

    window.addEventListener("resize", function () {
        for (let i = 0; i < blocks.length; i++) {

            let current = blocks[i];

            if (screen.width > breakpoint) {
                current.classList.remove("col-6");
                current.classList.add("col-sm-12");
                current.querySelector(".card__price-block").style.fontSize = "";
                current.querySelector(".card__model").style.fontSize = "";
                current.querySelector(".card__brand").style.fontSize = "";
            }
        }
    })

    function toggleCardSize(e) {
        for (let i = 0; i < blocks.length; i++) {
            let current = blocks[i];

            if (current.classList.contains("col-sm-12")) {
                current.classList.remove("col-sm-12");
                current.classList.add("col-6");
                current.querySelector(".card__price-block").style.fontSize = 1 + "rem";
                current.querySelector(".card__model").style.fontSize = 1 + "rem";
                current.querySelector(".card__brand").style.fontSize = 1.5 + "rem";
            } else {
                current.classList.remove("col-6");
                current.classList.add("col-sm-12");
                current.querySelector(".card__price-block").style.fontSize = "";
                current.querySelector(".card__model").style.fontSize = "";
                current.querySelector(".card__brand").style.fontSize = "";
            }
        }
    }
});

//display filters
document.addEventListener("DOMContentLoaded", function () {
    let btnBox = document.querySelector(".js-box-filter"),
        filterBox = document.querySelector(".filters__sets")

    if (!btnBox) return;

    let filter = btnBox.querySelector(".filters__btn[data-filter=\"filter\"]").dataset.filter;
    let search = btnBox.querySelector(".filters__btn[data-filter=\"search\"]").dataset.filter;

    btnBox.addEventListener("click", toggleFilter);

    window.addEventListener("resize", function () {
        filterBox.querySelector('div[data-target=\"' + filter + '\"]').style.maxHeight = "";
        filterBox.querySelector('div[data-target=\"' + filter + '\"]').classList.remove("show");
        filterBox.querySelector('div[data-target=\"' + search + '\"]').style.maxHeight = "";
        filterBox.querySelector('div[data-target=\"' + search + '\"]').classList.remove("show");
    });

    function toggleFilter(e) {
        let target = e.target;

        if (!target.closest(".filters__btn") || target.closest(".filters__menu")) return;

        let attr = target.dataset.filter,
            filter = filterBox.querySelector('div[data-target=\"' + attr + '\"]');

        if (filter.classList.contains('show')) {
            filter.classList.remove('show');
            filter.style.maxHeight = "";
            return;
        }

        filter.classList.add('show');
        filter.style.maxHeight = filter.scrollHeight + "px";
    }
});

// item card
document.addEventListener("DOMContentLoaded", function () {
    let itemForm = document.getElementById('order');
    
    if (!itemForm) {
        return;
    }
    
    itemForm.addEventListener('keypress', function(e){
        if (e.key === "Enter") { 
          e.preventDefault();
          return false;
        }
    });

    var orderBtn = document.querySelector('.btn-order'),
        cartPreview = document.querySelector('.order-number');

    orderBtn.addEventListener('click', function(){
        
        cartPreview.classList.add('shaker');
        setTimeout(function(){
            cartPreview.classList.remove('shaker')
        }, 820);
    });
})

//counter items
// document.addEventListener("DOMContentLoaded", function () {
//     let counter = document.querySelector(".js-count-items");

//     if (!counter) return;

//     let btns = counter.querySelectorAll('.js-count-items button'),
//         inputs = counter.querySelectorAll('.js-count-items input');

//     btns.forEach(function(btn){
//         btn.addEventListener('click', function(){
// 			var valueEl = this.closest('.js-count-items').querySelector('input');

// 			if (this.classList.contains('js-count-decrease')){
// 				if (valueEl.value != valueEl.getAttribute('min')){
// 					valueEl.value = parseInt(valueEl.value) - 1;
// 				}
// 			} else {
// 				if (valueEl.value != valueEl.getAttribute('max')){
// 					valueEl.value = parseInt(valueEl.value) + 1;
// 				}
// 			}

// 			var event = new Event('change');
// 			valueEl.dispatchEvent(event);
// 		});
//     });

//     inputs.forEach(function(input){
//         input.addEventListener('keyup', function(){
            
//             var _input = this;

//             if (!_input.value){
//                 _input.value = _input.getAttribute('min');
//             } else {

//                 if ( parseInt(_input.value) < parseInt(_input.getAttribute('min')) ){
//                     _input.value = _input.getAttribute('min');
//                 }
//                 if ( parseInt(_input.value) > parseInt(_input.getAttribute('max')) ){
//                     _input.value = _input.getAttribute('max');
//                 }
//             }
            
//         });
//     });
    
// });

//toggle sm-form (cooperation.html)
document.addEventListener("DOMContentLoaded", function () {
    let breakpoint = 991,
        form = document.querySelector(".form-block__form"),
        btn = document.querySelector(".js-form-toggler");

    if (!form || !btn) return;

    window.onresize = function () {
        if (document.documentElement.clientWidth > breakpoint) {
            btn.classList.remove("active");
            findFormVisibility();
        }
    }

    btn.addEventListener("click", toggleForm);

    findFormVisibility();

    function findFormVisibility() {
        if (document.body.clientWidth > breakpoint) {
            form.style.maxHeight = form.scrollHeight + "px";
        } else {
            form.style.maxHeight = "";
        }
    }

    function toggleForm() {
        if (document.body.clientWidth <= breakpoint) {
            if (!this.nextElementSibling.style.maxHeight) {
                this.classList.add("active");
                this.nextElementSibling.style.maxHeight = this.nextElementSibling.scrollHeight + "px";
                return;
            }
            this.classList.remove("active");
            this.nextElementSibling.style.maxHeight = "";
        }
    }
});

//Swiper item + change vendor & sliders functionality
document.addEventListener("DOMContentLoaded", function () {
    let colorsInputs = document.querySelectorAll(".js-color"),
        vendors = document.querySelectorAll(".specific__vendor"),
        sizes = document.querySelectorAll(".js-size"),
        swItem = null;

    if (!colorsInputs.length || !vendors.length || !sizes.length) return;

    activateAll();

    for (let i = 0; i < document.querySelectorAll(".js-color-label").length; i++) {
        let current = document.querySelectorAll(".js-color-label")[i];

        current.addEventListener("click", activateAll);
    }

    for (let i = 0; i < document.querySelectorAll(".js-size-label").length; i++) {
        let current = document.querySelectorAll(".js-size-label")[i];

        current.addEventListener("click", activateAll);
    }

    function getActiveAttrs() {

        let attrs = [];

        for (let i = 0; i < colorsInputs.length; i++) {
            let current = colorsInputs[i];
            if (current.checked) {
                let color = current.nextElementSibling.dataset.color;
                attrs.push(color);
                break;
            }
        }

        for (let i = 0; i < sizes.length; i++) {
            let current = sizes[i];
            if (current.checked) {
                let size = current.nextElementSibling.dataset.size;
                attrs.push(size);
                break;
            }
        }

        return attrs;
    }

    function activateAll() {
        deactivateAll();
        let attrs = getActiveAttrs();

        //slider
        let slider = document.querySelector('.js-sw-change[data-slider=\"' + attrs[0] + '\"]');

        slider.classList.add("active");
        swItem = new Swiper('.js-sw-change[data-slider=\"' + attrs[0] + '\"]', {
            direction: 'horizontal',
            autoHeight: true,
            freeMode: false,

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        //Vendor
        let vendorsByColor = document.querySelectorAll('.specific__vendor--color[data-target-color=\"' + attrs[0] + '\"]');

        for (let i = 0; i < vendorsByColor.length; i++) {
            let vendor = vendorsByColor[i].querySelector('.specific__vendor--size[data-target-size=\"' + attrs[1] + '\"]');
            vendor.classList.add("active");
        }
    }

    function deactivateAll() {
        let sliders = document.querySelectorAll('.js-sw-change');
        for (let i = 0; i < sliders.length; i++) {
            sliders[i].classList.remove("active");
        }

        swItem = null;

        let vendors = document.querySelectorAll(".specific__vendor--size");
        for (let i = 0; i < vendors.length; i++) {
            vendors[i].classList.remove("active");
        }
    }
});

//animation on main page
document.addEventListener("DOMContentLoaded", function () {

    let link = document.querySelector(".js-image"),
        width = document.documentElement.clientWidth;

    if (!link) return;

    window.addEventListener("resize", calculateNewLeftCoord);
    window.addEventListener("scroll", preventScroll);

    link.style.opacity = 1;
    link.style.width = 165 + "px";
    link.style.top = 8.1 + "rem";
    link.style.left = width / 2 - 83 + "px";

    link.addEventListener("transitionend", function () {
        link.parentNode.remove();
        window.removeEventListener("scroll", preventScroll);
    });

    function preventScroll(e) {
        if (document.querySelector(".header-js-animation")) {
            this.scroll(0, 0);
            e.preventDefault();
        }
    }

    function calculateNewLeftCoord() {
        width = document.documentElement.clientWidth;
        link.style.left = width / 2 - 83 + "px";
    }
});

//Toggle sidebar sub-menu
document.addEventListener("DOMContentLoaded", function () {
    let sidebar = document.querySelector(".sidebar"),
        item;

    if (!sidebar) return;

    // sidebar.addEventListener("click", toggleSubMenu);

    // function toggleSubMenu(e) {
    //     let target = e.target;

    //     if (target.closest(".sidebar__link")) {
    //         if (target.parentNode.querySelector(".sub-catalog")) {
    //             e.preventDefault();
    //             deactiveteAllElements(target);
    //             target.parentNode.classList.toggle("active");
    //             item = target.parentNode;
    //         }
    //     }
    // }

    function deactiveteAllElements(target) {
        let allItems = sidebar.querySelectorAll(".sidebar__link");

        for (let i = 0; i < allItems.length; i++) {
            if (target.parentNode === item) {
                continue;
            }

            allItems[i].parentNode.classList.remove("active");
        }
    }
})

//Add sticky top menu
document.addEventListener("DOMContentLoaded", function () {
    let header = document.querySelector("header.header:not(.animate__animated)");

    if (!header) {
        return;
    }
    
    let headerHeight = header.offsetHeight,
        navHeight = 0,
        scrollPos;

    window.addEventListener("scroll", addStickyMenu);

    function addStickyMenu() {
        let coordY = window.pageYOffset;

        if(header.classList.contains("sticky")){
            navHeight = document.querySelector(".sticky").getBoundingClientRect().height;
        }

        if (scrollPos < coordY) {
            if (scrollPos > headerHeight + navHeight) {
                header.classList.add("sticky");
                addAnimation("animate__animated", "animate__slideInDown");
            }
        }

        if (scrollPos > coordY) {
            if (coordY <= headerHeight - navHeight && header.classList.contains("sticky")) {
                removeAnimation("animate__animated", "animate__slideInDown");
                header.classList.remove("sticky");
            }
        }
        scrollPos = coordY;
    }

    function removeAnimation(animation, animationStyle) {
        if (header.classList.contains(animationStyle) && header.classList.contains(animation)) {
            header.classList.remove(animation);
            header.classList.remove(animationStyle);
        }
    }

    function addAnimation(animation, animationStyle) {
        if (!header.classList.contains(animationStyle) && !header.classList.contains(animation)) {
            header.classList.add(animation);
            header.classList.add(animationStyle);
        }
    }
});

//Link in link on catalog page
document.addEventListener("DOMContentLoaded", function () {
    let catalog = document.querySelectorAll(".catalog");

    if (!catalog.length) return;

    for (let i = 0; i < catalog.length; i++) {
        catalog[i].addEventListener("click", openCatalog);
    }

    function openCatalog(e) {
        if (e.target.tagName !== "A") {
            window.location.href = this.dataset.url;
        }
    }
})


// change sw-menu bg on hover
// document.addEventListener('DOMContentLoaded', function(){

//     var links = document.querySelectorAll('.sw-menu__image-wrapper .brands__link');
//     if (!links.length) {
//         return;
//     }

//     links.forEach(function(link){
//         link.addEventListener('mouseover', function(){
//             let wrp = this.closest('.sw-menu__image-wrapper');
//             wrp.style.backgroundImage = 'url("'+this.dataset.img+'")';
//         });
//     })
// })


/*
* Cookie warning 
*/
function setCookie(name, value, options) {
	options = options || {};

	var expires = options.expires;

	if (typeof expires == "number" && expires) {
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
		options.expires = expires.toUTCString();
	}

	value = encodeURIComponent(value);

	var updatedCookie = name + "=" + value;

	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}

	document.cookie = updatedCookie;
}

function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

document.addEventListener('DOMContentLoaded', function(){
	const btn = document.querySelector('.cow__close');
	if (btn) {
		const	win = document.querySelector('.cow'),
                isShowed = getCookie('cookieShowed');

		if (!isShowed){
			win.style.display = 'flex';

			btn.addEventListener('click', function(){
				win.style.display = 'none';
				setCookie('cookieShowed', 1, {expires: 60 * 60 * 24 * 14});
			});
		}
	}

});
// import Swiper from 'swiper';

//First slider
document.addEventListener("DOMContentLoaded", function () {
    let slider = document.querySelector(".swiper-main");

    if (!slider) return;

    const slidesCount = slider.querySelector('.js-swiper-count'),
          currentSlide = slider.querySelector('.js-slide-count');

    let swiperMain = new Swiper('.swiper-main', {
        autoHeight: true,
        loop: true,
        autoplay: {
            delay: 9000,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        on: {
            init: function(){
                slidesCount.textContent = this.slides.length - 2;
            },
            slideChange: function(){
                currentSlide.textContent = this.realIndex + 1;
            }
        }
    });
});

//Swiper - menu
document.addEventListener("DOMContentLoaded", function () {
    let items = document.querySelectorAll(".sw-menu__item"),
        allSliders = document.querySelectorAll(".sw-menu__slider"),
        allImages = document.querySelectorAll(".sw-menu__image-wrapper");
    let swMenu = null;

    if (!items.length || !allSliders.length || !allImages.length) return;

    showActiveItems();

    for (let i = 0; i < items.length; i++) {
        items[i].addEventListener("click", activateLink)
    }

    function activateLink() {
        deactivateAll();

        this.classList.add("active");

        getActiveAttr();
        showActiveItems();
    }

    function deactivateAll() {
        swMenu = null;
        for (let i = 0; i < items.length; i++) {
            items[i].classList.remove("active")
        }
        for (let i = 0; i < allImages.length; i++) {
            allImages[i].classList.remove("active")
        }
        for (let i = 0; i < allSliders.length; i++) {
            allSliders[i].classList.remove("active")
        }
    }

    function getActiveAttr() {
        let attr = "";
        for (let i = 0; i < items.length; i++) {
            let current = items[i];

            if (current.classList.contains("active")) {
                attr = current.dataset.item;
            }
        }
        return attr;
    }

    function showActiveItems() {
        let activeAttr = getActiveAttr();
        let imageBlock = document.querySelector('.sw-menu__image-wrapper[data-target=\"' + activeAttr + '\"]');
        let slider = document.querySelector('.sw-menu__slider[data-slider=\"' + activeAttr + '\"]');

        slider.classList.add("active");
        imageBlock.classList.add("active");
        swMenu = new Swiper('.sw-menu__slider[data-slider=\"' + activeAttr + '\"]', {
            direction: 'horizontal',
            freeMode: false,
            autoplay: {
                delay: 9000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                450: {slidesPerView: 2, spaceBetween: 20},
                768: {slidesPerView: 3, spaceBetween: 20},
                991: {slidesPerView: 1, spaceBetween: 0},
            },
            slidesPerView: 1,
            spaceBetween: 0,
        })
    }
});


//Swiper - news
const newsEl = document.querySelector(".news__slider");
if (newsEl) {
    let swiperNews = new Swiper(newsEl, {
        direction: 'horizontal',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
        },
        breakpoints: {
            500: {slidesPerView: 2, spaceBetween: 20},
            991: {slidesPerView: 3, spaceBetween: 20},
            1199: {slidesPerView: 4, spaceBetween: 20},
            1800: {slidesPerView: 5, spaceBetween: 20},
        },
        slidesPerView: 1,
        spaceBetween: 30,
    });
}

// news on main
document.addEventListener("DOMContentLoaded", function () {

    let slider = document.querySelector(".news-slider");

    if (!slider) return;

    let swiperNews = new Swiper(slider, {
        autoplay: {
            delay: 15000,
        },
        navigation: {
            nextEl: slider.querySelector('.slider-btn--next'),
            prevEl: slider.querySelector('.slider-btn--prev'),
        },
    });
});


var itemSwiper;
function itemSwiperUpdate(){
    itemSwiper.update();
}

document.addEventListener("DOMContentLoaded", function () {

    var itemSlider = document.querySelector('.swiper-item:not(.js-sw-change), .swiper-item-twin:not(.js-sw-change)')

    if (!itemSlider) {
        return;
    }
    
    itemSwiper = new Swiper(itemSlider, {
        navigation: {
            nextEl: itemSlider.querySelector('.swiper-button-next'),
            prevEl: itemSlider.querySelector('.swiper-button-prev'),
        },
    });
});
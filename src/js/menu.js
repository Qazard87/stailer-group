"use strict";

document.addEventListener("DOMContentLoaded", function () {
    let menu = document.querySelector(".header .nav");

    if (!menu) {
        return;
    }
    
    let items = menu.querySelectorAll(".nav__item"),
        burger = document.querySelector("#toggle"),
        breakpoint = 991,
        curr = null;

    chooseListeners();

    function chooseListeners() {
        menu.addEventListener("click", toggleSmSubMenu);

        if (document.documentElement.clientWidth > breakpoint) {
            menu.addEventListener("mouseover", findLgSubMenu);
            menu.addEventListener("mouseout", removeLgSubMenu);
        } else {
            burger.addEventListener("click", toggleSmMenu);
        }
    }

    function removeAllListeners() {
        menu.removeEventListener("mouseover", findLgSubMenu);
        menu.removeEventListener("mouseout", removeLgSubMenu);
        for (let i = 0; i < items.length; i++) {
            items[i].removeEventListener("click", toggleSmSubMenu)
        }
    }

    window.addEventListener("resize", function (e) {
        if (document.documentElement.clientWidth > breakpoint) {
            burger.classList.remove("cross");
            removeAllSmSubMenu();
        }
        removeAllListeners();
        chooseListeners();
    })

    //sm-menu functions
    function toggleSmMenu() {
        toggleBurgerIcon();
        removeAllSmSubMenu();
        menu.classList.toggle("show");
    }

    function toggleBurgerIcon() {
        burger.classList.toggle("cross");
    }

    function toggleSmSubMenu(e) {

        let target = e.target.closest(".nav__item"),
            subMenu = target.querySelector(".sub-menu");

        if (!subMenu) return;

        if (!subMenu.style.maxHeight) {
            subMenu.style.maxHeight = subMenu.scrollHeight + "px";
            subMenu.parentNode.classList.add("active");
            window.onscroll = function () {
                return false;
            }
        } else {
            subMenu.style.maxHeight = "";
            subMenu.parentNode.classList.remove("active");
        }

    }

    function removeAllSmSubMenu() {
        let subMenu = document.querySelectorAll(".sub-menu");

        for (let i = 0; i < subMenu.length; i++) {
            subMenu[i].parentNode.classList.remove("active");
            subMenu[i].style.maxHeight = "";
        }
    }

    //lg-menu functions
    function findLgSubMenu(e) {
        if (curr && curr != this) return;

        var target = e.target.closest('.nav__item');
        if (!target) return;

        var subMenu = target.querySelector(".sub-menu");

        if (curr == this) {
            removeLgSubMenu(e, target);
            curr = target;

            if (!subMenu) {
                removeLgSubMenu(e, target);
                return;
            }

            showLgSubMenu(target);
            return;
        }
        if (!subMenu) return;

        curr = target;
        showLgSubMenu(target);
    }

    function removeLgSubMenu(e, node) {
        if (!curr) return;

        var related = e.relatedTarget;

        while (related) {
            if (related == node) {
                curr = node;
                return;
            }

            related = related.parentNode;
        }

        removeAllSmSubMenu();
        curr = null;
    }

    function showLgSubMenu(node) {
        node.classList.add("active");
        node.querySelector(".sub-menu").style.maxHeight = node.querySelector(".sub-menu").scrollHeight + "px";
    }
});

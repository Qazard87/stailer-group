var ClOSE_TIMER = 15,
    remaining,
    countdownInterval;
    
/*
* Common functions
*/
document.addEventListener('DOMContentLoaded', function(){
  var closeBtns = document.querySelectorAll('.modal__link--close-popup');

  if (closeBtns.length) {

    [].forEach.call(closeBtns, function(closeBtn){

      closeBtn.addEventListener('click', function(){
        this.closest('.modal').classList.remove('show');
        clearInterval(countdownInterval);
      });
    });
  }

  /*
  * Show comment
  */
  document.addEventListener('click', function(e){
    var commentBtn = e.target.closest('.icon-btn--comment');
    var old = document.querySelector('.comment-popup__popup--active');
    if (old) {
      old.classList.remove('comment-popup__popup--active');
    }
    
    if (commentBtn) {
      commentBtn.parentNode.querySelector('.comment-popup__popup')
        .classList.add('comment-popup__popup--active');      
    }
  });
});


/*
* delete order popup
*/
document.addEventListener('DOMContentLoaded', function(){
  
  var deletePopup = document.querySelector('.modal--delete-order, .modal--delete');
  if (!deletePopup) {
    return;
  }

  var deleteBtns = document.querySelectorAll('.icon-btn--delete'),
      confirmDelete = deletePopup.querySelector('.button--black'),
      totalCountEl = document.querySelector('.pretty-table-footer__count .value');
  
  var timerEl = deletePopup.querySelector('.seconds');
  
  document.addEventListener('click', function(e){
    var btn = e.target.closest('.icon-btn--delete');
    
    if (!btn) {
      return;
    }
    
    deletePopup.dataId = btn.dataset.id;

    if (btn.hasAttribute('data-order')) {
      var deletePopupNumber = deletePopup.querySelector('.delete-order__number');
      deletePopupNumber.textContent = '№' + deletePopup.dataId;
    }

    if (btn.hasAttribute('data-employee')) {
      var wrp = btn.closest('tr'),
          employee = wrp.querySelector('.employee').textContent,
          branch = wrp.querySelector('.branch').textContent;
      
      deletePopup.querySelector('.modal__title--name').textContent = employee;
      deletePopup.querySelector('.modal__title--branch').textContent = branch;
    }

    initCodeResend();
    deletePopup.classList.add('show');
  });

  confirmDelete.addEventListener('click', function(){
    let url = "/assets/data/delete-order.json",
        data = new FormData();
    
    data.append('id', deletePopup.dataId);

    confirmDelete.disabled = true;

    let response = fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: data
    })
      .then(response => response.json())
      .then(result => {

        if (result.err === 0) {
          var target = document.querySelector('.icon-btn--delete[data-id="'+deletePopup.dataId+'"]').closest('tr');
          if (target){
            target.remove();
          }

          confirmDelete.disabled = false;
          deletePopup.classList.remove('show');
          totalCountEl.textContent = result.totalCount;
        }
      })
  });

  function countdown(remaining) {
    timerEl.textContent = remaining;

    if (remaining === 0) {
      deletePopup.classList.remove('show');
      clearInterval(countdownInterval);
      return;
    }
  }

  function initCodeResend(){
    remaining = ClOSE_TIMER;
    timerEl.textContent = remaining;

    countdownInterval = setInterval (function() {
      remaining -= 1;
      countdown(remaining);
    }, 1000);
  }
});


/*
* orders
*/
document.addEventListener('DOMContentLoaded', function(){
  var submitBtn = document.querySelector('.cart-footer__submit');
  if (!submitBtn) {
    return;
  }

  submitBtn.addEventListener('click', function(){

    let url = "/assets/data/order-create.html",
        data = new FormData();

        submitBtn.disabled = true;

    let response = fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: data
    })
    .then(response => response.text())
    .then(response => {
      document.body.insertAdjacentHTML('beforeEnd', response);
      console.log('document.body: ', document.body);
    });
  });

  
  // clear cart
  var clearBtn = document.querySelector('.cart-footer__link--clear'),
      emptyCartPopup = document.querySelector('.modal--empty-cart'),
      timerEl = emptyCartPopup.querySelector('.seconds');
  clearBtn.addEventListener('click', function(){
    emptyCartPopup.classList.add('show');

    function countdown(remaining) {
      timerEl.textContent = remaining;
  
      if (remaining === 0) {
        emptyCartPopup.classList.remove('show');
        clearInterval(countdownInterval);
        return;
      }
    }  
    
    remaining = ClOSE_TIMER;
    timerEl.textContent = remaining;

    countdownInterval = setInterval (function() {
      remaining -= 1;
      countdown(remaining);
    }, 1000);

  });
});


/*
* employees
*/
document.addEventListener('DOMContentLoaded', function(){

  var popup = document.querySelector('.modal--employee');
  if (!popup) {
    return;
  }

  var newEmpBtn = document.querySelector('[data-new-employee-btn]'),
      empForm = popup.querySelector('.e-form'),
      titleNew = popup.querySelector('.modal__title--new'),
      titleEdit = popup.querySelector('.modal__title--edit');

  document.addEventListener('click', function (e){
    var edit = e.target.closest('.icon-btn--edit');

    if (edit) {
      var line = edit.closest('tr'),
          name = line.querySelector('.employee').textContent,
          branch = line.querySelector('.branch').textContent,
          tel = line.querySelector('.tel').textContent,
          email = line.querySelector('.email').textContent,
          price = line.querySelector('.price').checked,
          checkout = line.querySelector('.checkout').checked;

      empForm.querySelector('.e-form__name').value = name;
      empForm.querySelector('.e-form__branch').value = branch;
      empForm.querySelector('.e-form__tel').value = tel;
      empForm.querySelector('.e-form__email').value = email;
      empForm.querySelector('.e-form__price').checked = price;
      empForm.querySelector('.e-form__checkout').checked = checkout;

      titleNew.style.display = 'none';
      titleEdit.style.display = '';
      popup.classList.add('show');
    }
  });


  newEmpBtn.addEventListener('click', function(){
    empForm.reset();
    popup.classList.add('show');

    titleEdit.style.display = 'none';
    titleNew.style.display = '';
  });
})


/*
* Submit form
*/
document.addEventListener('DOMContentLoaded', function(){

  var form = document.querySelector('.c-form');

  if (!form) {
    return;
  }  

  var telInput = document.querySelector('.input-line__input--tel');
  if (telInput) {
    IMask(
      telInput, {
          mask: '+{7}(000)000-00-00'
      }
    )
  }

  var requireElements = form.querySelectorAll('[required]'),
      submitBtn = form.querySelector('.c-form__submit'),
      state = submitBtn.textContent;

  function clearFormErrors() {
    let texts = form.querySelectorAll('.c-form__inline-error:not(.hidden)');
    texts.forEach(function(text){
      text.classList.add('hidden');
    });

    let styles = form.querySelectorAll('.has-error');
    styles.forEach(function(style){
      style.classList.remove('has-error');
    });
  }

  function removeSingleErrorNode(){
    let isErrorNode = this.classList.contains('has-error');

    if (isErrorNode) {
      this.classList.remove('has-error');
      this.nextElementSibling.classList.add('hidden');
    }
  }

  function scrollToFirstError() {
    let errEl = form.querySelector('.has-error');

    if (errEl) {
      errEl.closest('.input-line').scrollIntoView({
        behavior: 'smooth'
      });
    }
  }

  requireElements.forEach(function(el){
    el.addEventListener('input', removeSingleErrorNode);
  });



  form.addEventListener('submit', function(e){
    e.preventDefault();

    clearFormErrors();

    let bError = false;

    requireElements.forEach(function(el){
      
      if (!el.checkValidity()) {
        let errorType = el.validity.valueMissing ? 'valueMissing' : 'valueInvalid';
        let errorEl = el.parentNode.querySelector('.c-form__inline-error');
        if (errorEl) {
          errorEl.innerHTML = el.dataset[errorType];
          errorEl.classList.remove('hidden');
        }
        el.classList.add('has-error');

        bError = true;
      }
    });
    
    if (bError) {
      scrollToFirstError();
      return;
    }

    let request = new XMLHttpRequest();
    request.responseType = 'json';
    request.open(form.getAttribute('method'), form.getAttribute('action'), true);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');

    request.addEventListener("readystatechange", function (e) {

      if (request.readyState === 4 && request.status === 200) {

        if (!this.response.err) {
          let sucessEls = document.querySelectorAll('.success-hide');
          sucessEls.forEach(function(el){
            el.remove();
          })

          let p = document.createElement('p');
          p.classList.add('c-form__text-success');
          p.textContent = this.response.elements[0].message;
          form.appendChild(p);
        } else {

          this.response.elements.forEach(function(el){
            let errEl = document.querySelector('[name="'+el.errName+'"]');
            if (errEl) {
              errEl.classList.add('has-error');
              let errText = errEl.nextElementSibling;
              errText.textContent = el.message;
              errText.classList.remove('hidden');
            }
          });

          scrollToFirstError();
          submitBtn.disabled = false;
        }

        submitBtn.textContent = state;
      }
    });

    submitBtn.disabled = true;
    submitBtn.textContent = submitBtn.dataset.wait;

    data = new FormData(form);
    request.send(data);
  });


  /*
  * new pass validation
  */
  var newPassInputs = document.querySelectorAll('.new-pass-input');
  newPassInputs.forEach(function(input){

    input.addEventListener('change', function(){
      
      if (newPassInputs[0].value === newPassInputs[1].value) {
        newPassInputs[1].setCustomValidity('');
      } else {
        newPassInputs[1].setCustomValidity(newPassInputs[1].dataset.valueInvalid);
      }
    });

  });
});


/*
* Search panel
*/
document.addEventListener('DOMContentLoaded', function(){

  var searchInput = document.querySelector('.search-line__input');
  if (!searchInput) {
    return;
  }

  let request = new XMLHttpRequest(),
      dataContainer = document.querySelector('.data-container'),
      preloader = document.querySelector('.search-preloader');

  searchInput.addEventListener('input', function(){
    var val = searchInput.value;

    if (typeof callApiRequest != 'undefined' && request.readyState != 4){
      request.abort();
    }      
    
    request.open('post', searchInput.dataset.action, true);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');

    request.addEventListener("readystatechange", function (e) {
      if (this.readyState != 4) return;

      dataContainer.innerHTML = this.response;

      dataContainer.style.display = '';
      preloader.style.display = '';
    });

    dataContainer.style.display = 'none';
    preloader.style.display = 'block';

    let data = 'value=' + searchInput.value;
    request.send(data);      
    
  });
});
"use strict";

import IMask from "imask";

//Modal news
document.addEventListener("DOMContentLoaded", function () {
    document.addEventListener("click", openModalNews);

    function openModalNews(e) {
        let newsAttr = getCurrentAttr(e);

        if (!newsAttr) return;

        let modal = document.querySelector('.modal-news[data-target-modal=\"' + newsAttr + '\"]');
        if (!modal) return;

        modal.classList.add("show");
        let closeBtn = modal.querySelector(".modal__close");
        closeBtn.addEventListener("click", closeModal);
    }

    function getCurrentAttr(e) {
        let target = e.target.closest(".js-modal-btn");

        if (!target) return;

        e.preventDefault();

        let attr = target.dataset.openModal;
        return attr;
    }

    function closeModal() {
        this.parentNode.parentNode.classList.remove("show");
        this.removeEventListener("click", closeModal);
    }
});

document.addEventListener("DOMContentLoaded", function () {
    let closeModalBtn = document.querySelectorAll(".modal__close"),
        loginModal = document.querySelector(".modal-login"),
        registerModalWarning = document.querySelector(".modal-register-warning"),
        registerModal = document.querySelector(".modal-register"),
        forgetPassModal = document.querySelector(".modal-forget-password");
        // email = loginModal.querySelector(".link__email"),
        // phone = loginModal.querySelector(".link__phone");
        //маска номера телефона
        // phoneMaskLogin = IMask(
        //     loginModal.querySelector('input[type="phone"]'), {
        //         mask: '+{7}(000)000-00-00'
        //     });

    //open modal listener
    document.addEventListener("click", selectModal);

    //close modal listeners
    for (let i = 0; i < closeModalBtn.length; i++) {
        closeModalBtn[i].addEventListener("click", function (e) {
            removeAllModals();
        })
    }

    function selectModal(e) {
        let target = e.target;

        if (target.closest(".js-login-modal")) {
            removeAllModals();
            loginModal.classList.add("show");
            // displayInputLogin();
            // email.addEventListener("click", chooseLogin);
            // phone.addEventListener("click", chooseLogin);
        }
        if (target.closest(".js-register-modal")) {
            removeAllModals();
            registerModalWarning.classList.add("show");
        }
        if (target.closest(".js-reg-open")) {
            removeAllModals();
            registerModal.classList.add("show")
        }
        if (target.closest(".js-forget-pass")) {
            removeAllModals();
            forgetPassModal.classList.add("show")
        }
    }

    function chooseLogin() {
        let links = document.querySelectorAll(".form__link");
        for (let i = 0; i < links.length; i++) {
            links[i].classList.remove("active")
        }
        this.classList.add("active");
        displayInputLogin();
    }

    function getActiveAttr() {
        let links = document.querySelectorAll(".form__link"),
            attr;
        for (let i = 0; i < links.length; i++) {
            if (links[i].classList.contains("active")) {
                attr = links[i].dataset.login
                break;
            }
        }
        return attr;
    }

    function displayInputLogin() {
        removeLoginInputs();
        let activeAttr = getActiveAttr();
        loginModal.querySelector('.js-login-input[data-target=\"' + activeAttr + '\"]').classList.remove("form__input--hidden");
    }

    function removeLoginInputs() {
        let inputs = loginModal.querySelectorAll('.js-login-input');

        for (let i = 0; i < inputs.length; i++) {
            inputs[i].classList.add("form__input--hidden");
        }
    }

    function removeAllModals() {
        removeAllSubListeners();

        let modals = document.querySelectorAll(".modal")

        for (let i = 0; i < modals.length; i++) {
            if (modals[i].classList.contains("show")) {
                modals[i].classList.remove("show");
            }
        }
    }

    function removeAllSubListeners() {
        // email.removeEventListener("click", chooseLogin);
        // phone.removeEventListener("click", chooseLogin);
    }
});

// common functions
document.addEventListener("DOMContentLoaded", function () {
    var closeBtns = document.querySelectorAll('.modal__close-btn');

    [].forEach.call(closeBtns, function(btn){
        btn.addEventListener('click', function(){
            btn.closest('.modal').classList.remove('show'); 
        });
    });


    var openBtns = document.querySelectorAll('[data-popup-target]');
    [].forEach.call(openBtns, function(btn){
        btn.addEventListener('click', function(){
            document.querySelector('.'+btn.dataset.popupTarget).classList.add('show');
        });
    });
});
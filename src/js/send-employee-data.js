"use strict";

import IMask from "imask";

document.addEventListener("DOMContentLoaded", function () {

    let employeeForm = document.querySelector(".js-employee-form.show");

    if (!employeeForm) return;

    let phone = employeeForm.querySelector('[type=\"phone\"]'),
        phMask = IMask(
            phone, {
                mask: '+{7}(000)000-00-00'
            }),
        form = employeeForm.querySelector('form'),
        checkboxes = employeeForm.querySelectorAll("input[type=\"checkbox\"]"),
        inputs = employeeForm.querySelectorAll("input:not([type=\"checkbox\"]):not([type=\"submit\"]):not([type=\"reset\"]):not([type=\"button\"])"),
        email = employeeForm.querySelector("input[name=\"email\"]"),
        selects = employeeForm.querySelectorAll("select"),
        btnSubmit = employeeForm.querySelector("input[type=\"submit\"]"),
        violations = 0;

    checkTogglerCheckboxPosition();

    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener("change", toggleCheckboxHelper);
    }

    for (let i = 0; i < selects.length; i++) {
        selects[i].addEventListener("input", removeSingleErrorNode);
    }

    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("input", removeSingleErrorNode);
    }

    employeeForm.addEventListener("submit", fetchForm)

    function toggleCheckboxHelper() {
        choosePosition(this);
    }

    function checkTogglerCheckboxPosition() {
        for (let i = 0; i < checkboxes.length; i++) {
            choosePosition(checkboxes[i])
        }
    }

    function choosePosition(node){
        let thumb = node.nextElementSibling.querySelector(".js-thumb");

        if (node.checked) {
            thumb.style.left = node.nextElementSibling.getBoundingClientRect().width - thumb.getBoundingClientRect().width + "px";
            node.nextElementSibling.style.backgroundColor = "green";
        } else {
            thumb.style.left = "";
            node.nextElementSibling.style.backgroundColor = "";
        }
    }

    function fetchForm(e) {
        e.preventDefault();
        removeAllErrors();
        validateForm();

        if (violations > 0) return;

        toggleSubmitBtnDisable();
        fetchData();
    }

    function fetchData() {
        let url = "/assets/data/employee-success.json",
            data = new FormData(form);

        let response = fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: data
        })
            .then(response => response.json())
            .then(result => {
                if (result.err === 0) {
                    let modals = document.querySelectorAll(".modal"),
                        modalSuccess = document.querySelector(".modal-status");
                    for (let i = 0; i < modals.length; i++) {
                        if (modals[i].classList.contains("show")) {
                            modals[i].classList.remove("show");
                        }
                    }
                    modalSuccess.classList.add("show");
                    modalSuccess.querySelector(".modal__title").innerHTML = result.elements[0].message
                    modalSuccess.querySelector("p.modal__info").innerHTML = result.elements[1].message
                    toggleSubmitBtnDisable();
                    return;
                } else {
                    for (let i = 0; i < result.elements.length; i++) {

                        let message = result.elements[i].message,
                            errName = result.elements[i].errName;

                        for (let j = 0; j < form.elements.length; j++) {
                            let current = form.elements[j];

                            if (errName === current.name) {
                                createErrorNode(current, message);
                            }
                        }
                    }
                    toggleSubmitBtnDisable();
                }
            })
    }

    function validateForm() {
        violations = 0;
        validateInputs();
        validateEmail();
        validatePhone();
        validateSelects();
    }

    function validateInputs() {
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].name === "email" || inputs[i].name === "phone") continue;
            if (!inputs[i].value) {
                createErrorNode(inputs[i], "Поле обязательно для заполнения")
                violations++;
            }
        }
    }

    function validatePhone() {
        if (!phone.value) {
            createErrorNode(phone, "Поле обязательно для заполнения")
            violations++;
            return;
        }

        if (phone.value.length !== 16) {
            createErrorNode(phone, "Некорректный номер телефона");
            violations++;
            return;
        }

    }

    function validateSelects() {
        for (let i = 0; i < selects.length; i++) {
            if (!selects[i].value) {
                createErrorNode(selects[i], "Поле обязательно для заполнения")
                violations++;
            }
        }
    }

    function validateEmail() {
        let regExp = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

        if (!email.value) {
            createErrorNode(email, "Поле обязательно для заполнения")
            violations++;
            return;
        }

        if (email.value.match(regExp) === null) {
            createErrorNode(email, "Некорректный email");
            violations++;
            return;
        }

    }

    function toggleSubmitBtnDisable() {
        btnSubmit.disabled = !btnSubmit.disabled;

        let newLabel = btnSubmit.dataset.wait;
        btnSubmit.dataset.wait = btnSubmit.value;
        btnSubmit.value = newLabel;
    }

    function createErrorNode(current, message) {
        let p = document.createElement("p");
        p.classList.add("text-error");
        current.classList.add("border-error")
        p.innerHTML = message;
        current.after(p);
    }

    function removeSingleErrorNode() {
        let isErrorNode = this.classList.contains("border-error");

        if (isErrorNode) {
            this.classList.remove("border-error");
            this.nextElementSibling.remove();
        }
    }

    function removeAllErrors() {
        let inputs = employeeForm.querySelectorAll("input"),
            selects = employeeForm.querySelectorAll("select"),
            p = employeeForm.querySelectorAll(".text-error");

        if (!p.length) return;

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].classList.contains("border-error")) {
                inputs[i].classList.remove("border-error");
                inputs[i].nextElementSibling.remove();
            }
        }

        for (let i = 0; i < selects.length; i++) {
            if (selects[i].classList.contains("border-error")) {
                selects[i].classList.remove("border-error");
                selects[i].nextElementSibling.remove();
            }
        }
    }
})
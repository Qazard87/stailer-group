/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ 968:
/***/ (function(__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./src/js/main.js
var main = __webpack_require__(927);
// EXTERNAL MODULE: ./src/js/menu.js
var menu = __webpack_require__(722);
// EXTERNAL MODULE: ./src/js/accordeon.js
var accordeon = __webpack_require__(604);
// EXTERNAL MODULE: ./src/js/swipers.js
var swipers = __webpack_require__(28);
// EXTERNAL MODULE: ./node_modules/imask/esm/index.js + 25 modules
var esm = __webpack_require__(113);
;// CONCATENATED MODULE: ./src/js/form-recording.js

document.addEventListener("DOMContentLoaded", function () {
  var form = document.querySelector("#partners-form-blank");
  if (!form) return; //маска номера телефона

  var phoneMask = (0,esm/* default */.ZP)(form.querySelector('#phone-mask'), {
    mask: '+{7}(000)000-00-00'
  }),
      btnSubmit = form.querySelector("#submit-partners-blank"),
      address = form.querySelectorAll(".js-address"),
      count = 0,
      agreeTerms = form.querySelector("#agree_terms"); //Обработчики событий

  agreeTerms.addEventListener("change", checkAgreeTerms);
  form.addEventListener('submit', fetchForm);

  for (var i = 0; i < form.querySelectorAll("input").length; i++) {
    var input = form.querySelectorAll("input")[i];
    input.addEventListener("input", removeSingleErrorNode);
  }

  form.querySelector(".js-add-address").addEventListener("click", addNewAddress); //Функции обработчиков

  function validateForm() {
    removeErrorNodes();
    var inputs = form.querySelectorAll("input[type='text']"),
        email = form.querySelectorAll("input[name='email']"),
        shopNumber = form.querySelectorAll("input[type='number']"),
        violations = 0;

    for (var _i = 0; _i < inputs.length; _i++) {
      if (inputs[_i].name !== "email") {
        if (inputs[_i].value.trim() === "") {
          createNoticeNode(inputs[_i], "text-error", "Пожалуйста, заполните поле");
          violations++;
        }
      }
    }

    for (var _i2 = 0; _i2 < email.length; _i2++) {
      if (email[_i2].value.trim() === "") {
        createNoticeNode(email[_i2], "text-error", "Пожалуйста, заполните поле");
        violations++;
        continue;
      }

      var regExp = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

      if (email[_i2].value.match(regExp) === null) {
        createNoticeNode(email[_i2], "text-error", "Некорректный email");
        violations++;
      }
    }

    for (var _i3 = 0; _i3 < shopNumber.length; _i3++) {
      var _regExp = /\d+/;

      if (shopNumber[_i3].value.match(_regExp) === null) {
        createNoticeNode(shopNumber[_i3], "text-error", "Некорректное число магазинов");
        violations++;
        continue;
      }

      if (shopNumber[_i3].value <= 0) {
        createNoticeNode(shopNumber[_i3], "text-error", "Значение поля должно быть положительным");
        violations++;
      }
    }

    if (!agreeTerms.checked) {
      createNoticeNode(agreeTerms, "text-error", "Поле обязательно для заполнения");
      violations++;
    }

    return violations;
  }

  function fetchForm(e) {
    e.preventDefault();
    var violations = validateForm();
    form.style.maxHeight = form.scrollHeight + "px";

    if (violations > 0) {
      scrollToFirstError();
      return;
    }

    toggleSubmitBtn();
    var url = "/assets/data/message-success.json",
        data = new FormData(form),
        request = new XMLHttpRequest();
    request.responseType = "json";
    request.open("post", url, true);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
    request.addEventListener("readystatechange", function (e) {
      if (request.readyState === 4 && request.status === 200) {
        if (this.response.err === 0 && this.response.elements.length === 1) {
          createSuccessMovements(btnSubmit, this.response.elements[0].message);
          form.style.maxHeight = form.scrollHeight + "px";
          return;
        }

        for (var _i4 = 0; _i4 < this.response.elements.length; _i4++) {
          var message = this.response.elements[_i4].message,
              errName = this.response.elements[_i4].errName;

          for (var j = 0; j < form.elements.length; j++) {
            var current = form.elements[j];

            if (errName === current.name) {
              createNoticeNode(current, "text-error", message);
            }
          }
        }

        scrollToFirstError();
      }

      toggleSubmitBtn();
    });
    request.send(data);
  }

  function scrollToFirstError() {
    var firstErr = document.querySelector(".border-error");
    if (!firstErr) return;
    var top = firstErr.getBoundingClientRect().top + pageYOffset,
        windowTop = document.body.scrollTop || window.pageYOffset,
        timer;

    if (top < windowTop) {
      timer = setInterval(function () {
        if (windowTop > top) window.scroll(0, windowTop -= 20);else clearInterval(timer);
      }, 5);
    } else {
      timer = setInterval(function () {
        if (windowTop < top) window.scroll(0, windowTop += 20);else clearInterval(timer);
      }, 5);
    }
  }

  function createNoticeNode(current, cls, message) {
    var p = document.createElement("p");
    p.classList.add(cls);

    if (cls === "text-error") {
      current.classList.add("border-error");
    }

    p.innerHTML = message;
    current.after(p);
  }

  function createSuccessMovements(current, message) {
    createNoticeNode(current, "text-success", message);
    btnSubmit.value = btnSubmit.dataset.wait;
  }

  function removeErrorNodes() {
    var inputs = form.querySelectorAll("input"),
        p = form.querySelectorAll(".text-error");
    if (!p.length) return;

    for (var _i5 = 0; _i5 < inputs.length; _i5++) {
      if (inputs[_i5].classList.contains("border-error")) {
        inputs[_i5].classList.remove("border-error");

        inputs[_i5].nextElementSibling.remove();
      }
    }
  }

  function removeSingleErrorNode() {
    if (this.type === "email") {
      this.removeAttribute("invalid");
    }

    var isErrorNode = this.classList.contains("border-error");

    if (isErrorNode) {
      this.classList.remove("border-error");
      this.nextElementSibling.remove();
    }
  }

  function toggleSubmitBtn() {
    btnSubmit.disabled = !btnSubmit.disabled;
    var newValue = btnSubmit.dataset.wait;
    btnSubmit.dataset.wait = btnSubmit.value;
    btnSubmit.value = newValue;
  }

  function checkAgreeTerms() {
    var label = this.parentNode.querySelector("label.form-block__label");

    if (this.checked) {
      label.classList.add("checked");
    } else label.classList.remove("checked");
  }

  function addNewAddress() {
    count++;
    var clone = this.previousElementSibling.cloneNode(true);

    if (clone.querySelector("input").classList.contains("border-error")) {
      clone.querySelector("input").classList.remove("border-error");
    }

    if (clone.querySelector(".text-error")) {
      clone.querySelector(".text-error").remove();
    }

    var nameAttr = "address-" + count;
    clone.querySelector("input").name = nameAttr;
    clone.querySelector("input").value = "";
    this.previousElementSibling.after(clone);
    addListenersForNodes();
    form.style.maxHeight = form.scrollHeight + "px";
  }

  function addListenersForNodes() {
    var addresses = form.querySelectorAll(".js-address input");

    for (var _i6 = 0; _i6 < addresses.length; _i6++) {
      addresses[_i6].addEventListener("input", removeSingleErrorNode);
    }
  }
});
;// CONCATENATED MODULE: ./src/js/modal.js


 //Modal news

document.addEventListener("DOMContentLoaded", function () {
  document.addEventListener("click", openModalNews);

  function openModalNews(e) {
    var newsAttr = getCurrentAttr(e);
    if (!newsAttr) return;
    var modal = document.querySelector('.modal-news[data-target-modal=\"' + newsAttr + '\"]');
    if (!modal) return;
    modal.classList.add("show");
    var closeBtn = modal.querySelector(".modal__close");
    closeBtn.addEventListener("click", closeModal);
  }

  function getCurrentAttr(e) {
    var target = e.target.closest(".js-modal-btn");
    if (!target) return;
    e.preventDefault();
    var attr = target.dataset.openModal;
    return attr;
  }

  function closeModal() {
    this.parentNode.parentNode.classList.remove("show");
    this.removeEventListener("click", closeModal);
  }
});
document.addEventListener("DOMContentLoaded", function () {
  var closeModalBtn = document.querySelectorAll(".modal__close"),
      loginModal = document.querySelector(".modal-login"),
      registerModalWarning = document.querySelector(".modal-register-warning"),
      registerModal = document.querySelector(".modal-register"),
      forgetPassModal = document.querySelector(".modal-forget-password"); // email = loginModal.querySelector(".link__email"),
  // phone = loginModal.querySelector(".link__phone");
  //маска номера телефона
  // phoneMaskLogin = IMask(
  //     loginModal.querySelector('input[type="phone"]'), {
  //         mask: '+{7}(000)000-00-00'
  //     });
  //open modal listener

  document.addEventListener("click", selectModal); //close modal listeners

  for (var i = 0; i < closeModalBtn.length; i++) {
    closeModalBtn[i].addEventListener("click", function (e) {
      removeAllModals();
    });
  }

  function selectModal(e) {
    var target = e.target;

    if (target.closest(".js-login-modal")) {
      removeAllModals();
      loginModal.classList.add("show"); // displayInputLogin();
      // email.addEventListener("click", chooseLogin);
      // phone.addEventListener("click", chooseLogin);
    }

    if (target.closest(".js-register-modal")) {
      removeAllModals();
      registerModalWarning.classList.add("show");
    }

    if (target.closest(".js-reg-open")) {
      removeAllModals();
      registerModal.classList.add("show");
    }

    if (target.closest(".js-forget-pass")) {
      removeAllModals();
      forgetPassModal.classList.add("show");
    }
  }

  function chooseLogin() {
    var links = document.querySelectorAll(".form__link");

    for (var _i = 0; _i < links.length; _i++) {
      links[_i].classList.remove("active");
    }

    this.classList.add("active");
    displayInputLogin();
  }

  function getActiveAttr() {
    var links = document.querySelectorAll(".form__link"),
        attr;

    for (var _i2 = 0; _i2 < links.length; _i2++) {
      if (links[_i2].classList.contains("active")) {
        attr = links[_i2].dataset.login;
        break;
      }
    }

    return attr;
  }

  function displayInputLogin() {
    removeLoginInputs();
    var activeAttr = getActiveAttr();
    loginModal.querySelector('.js-login-input[data-target=\"' + activeAttr + '\"]').classList.remove("form__input--hidden");
  }

  function removeLoginInputs() {
    var inputs = loginModal.querySelectorAll('.js-login-input');

    for (var _i3 = 0; _i3 < inputs.length; _i3++) {
      inputs[_i3].classList.add("form__input--hidden");
    }
  }

  function removeAllModals() {
    removeAllSubListeners();
    var modals = document.querySelectorAll(".modal");

    for (var _i4 = 0; _i4 < modals.length; _i4++) {
      if (modals[_i4].classList.contains("show")) {
        modals[_i4].classList.remove("show");
      }
    }
  }

  function removeAllSubListeners() {// email.removeEventListener("click", chooseLogin);
    // phone.removeEventListener("click", chooseLogin);
  }
}); // common functions

document.addEventListener("DOMContentLoaded", function () {
  var closeBtns = document.querySelectorAll('.modal__close-btn');
  [].forEach.call(closeBtns, function (btn) {
    btn.addEventListener('click', function () {
      btn.closest('.modal').classList.remove('show');
    });
  });
  var openBtns = document.querySelectorAll('[data-popup-target]');
  [].forEach.call(openBtns, function (btn) {
    btn.addEventListener('click', function () {
      document.querySelector('.' + btn.dataset.popupTarget).classList.add('show');
    });
  });
});
;// CONCATENATED MODULE: ./src/js/registration.js



document.addEventListener("DOMContentLoaded", function () {
  var formRegister = document.querySelector("#registration");
  if (!formRegister) return;
  var mask = new esm/* default */.ZP(formRegister.querySelector('input[type=\"phone\"]'), {
    mask: '+{0}(000)000-00-00'
  });
  var agreeTerms = formRegister.querySelector("#agree_terms_checkbox"),
      newsTerms = formRegister.querySelector("#agree_news"),
      inputs = formRegister.querySelectorAll("input[type=\"text\"]"),
      email = formRegister.querySelector("#regEmail"),
      phone = formRegister.querySelector("input[type=\"phone\"]"),
      selects = formRegister.querySelectorAll("select"),
      btnSubmit = formRegister.querySelector("#regBtn"),
      violations = 0; //Event listeners

  agreeTerms.addEventListener("change", checkAgreeTerms);
  newsTerms.addEventListener("change", checkNewsTerms);
  phone.addEventListener("input", removeSingleErrorNode);
  formRegister.addEventListener("submit", fetchForm);

  for (var i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("input", removeSingleErrorNode);
  }

  for (var _i = 0; _i < selects.length; _i++) {
    selects[_i].addEventListener("change", removeSingleErrorNode);
  } //Listeners functions


  function checkAgreeTerms() {
    var label = this.parentNode.querySelector(".form__label");

    if (this.classList.contains("border-error")) {
      this.classList.remove("border-error");
      this.nextElementSibling.remove();
    }

    if (this.checked) {
      label.classList.add("checked");
    } else label.classList.remove("checked");
  }

  function checkNewsTerms() {
    var label = this.parentNode.querySelector(".form__label");

    if (this.classList.contains("border-error")) {
      this.classList.remove("border-error");
      this.nextElementSibling.remove();
    }

    if (this.checked) {
      label.classList.add("checked");
    } else label.classList.remove("checked");
  }

  function createErrorNode(current, message) {
    var p = document.createElement("p");
    p.classList.add("text-error");
    current.classList.add("border-error");
    p.innerHTML = message;
    current.after(p);
  }

  function removeSingleErrorNode() {
    var isErrorNode = this.classList.contains("border-error");

    if (isErrorNode) {
      this.classList.remove("border-error");
      this.nextElementSibling.remove();
    }
  }

  function removeAllErrors() {
    var inputs = formRegister.querySelectorAll("input"),
        selects = formRegister.querySelectorAll("select"),
        p = formRegister.querySelectorAll(".text-error");
    if (!p.length) return;

    for (var _i2 = 0; _i2 < inputs.length; _i2++) {
      if (inputs[_i2].classList.contains("border-error")) {
        inputs[_i2].classList.remove("border-error");

        inputs[_i2].nextElementSibling.remove();
      }
    }

    for (var _i3 = 0; _i3 < selects.length; _i3++) {
      if (selects[_i3].classList.contains("border-error")) {
        selects[_i3].classList.remove("border-error");

        selects[_i3].nextElementSibling.remove();
      }
    }
  }

  function checkAgreeSelection() {
    if (!agreeTerms.checked) {
      createErrorNode(agreeTerms, "Поле обязательно для заполнения");
      violations++;
    }
  }

  function validateInputs() {
    for (var _i4 = 0; _i4 < inputs.length; _i4++) {
      if (inputs[_i4].name === "email") continue;

      if (!inputs[_i4].value) {
        createErrorNode(inputs[_i4], "Поле обязательно для заполнения");
        violations++;
      }
    }
  }

  function validateSelects() {
    for (var _i5 = 0; _i5 < selects.length; _i5++) {
      if (!selects[_i5].value) {
        createErrorNode(selects[_i5], "Поле обязательно для заполнения");
        violations++;
      }
    }
  }

  function validatePhone() {
    if (!phone.value) {
      createErrorNode(phone, "Поле обязательно для заполнения");
      violations++;
      return;
    }

    if (phone.value.length !== 16) {
      createErrorNode(phone, "Некорректный номер телефона");
      violations++;
      return;
    }
  }

  function validateEmail() {
    var regExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

    if (!email.value) {
      createErrorNode(email, "Поле обязательно для заполнения");
      violations++;
      return;
    }

    if (email.value.match(regExp) === null) {
      createErrorNode(email, "Некорректный email");
      violations++;
      return;
    }
  }

  function validateForm() {
    violations = 0;
    checkAgreeSelection();
    validateInputs();
    validateEmail();
    validatePhone();
    validateSelects();
  }

  function toggleSubmitBtnDisable() {
    btnSubmit.disabled = !btnSubmit.disabled;
    var newLabel = btnSubmit.dataset.wait;
    btnSubmit.dataset.wait = btnSubmit.value;
    btnSubmit.value = newLabel;
  }

  function fetchForm(e) {
    e.preventDefault();
    removeAllErrors();
    validateForm();
    if (violations > 0) return;
    toggleSubmitBtnDisable();
    fetchData();
  }

  function fetchData() {
    var url = "/assets/data/reg-success.json",
        data = new FormData(formRegister);
    var response = fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: data
    }).then(function (response) {
      return response.json();
    }).then(function (result) {
      if (result.err === 0) {
        var modals = document.querySelectorAll(".modal"),
            modalSuccess = document.querySelector(".modal-status");

        for (var _i6 = 0; _i6 < modals.length; _i6++) {
          if (modals[_i6].classList.contains("show")) {
            modals[_i6].classList.remove("show");
          }
        }

        modalSuccess.classList.add("show");
        modalSuccess.querySelector(".modal__title").innerHTML = result.elements[0].message;
        modalSuccess.querySelector("p.modal__info").innerHTML = result.elements[1].message;
        toggleSubmitBtnDisable();
        return;
      } else {
        for (var _i7 = 0; _i7 < result.elements.length; _i7++) {
          var message = result.elements[_i7].message,
              errName = result.elements[_i7].errName;

          for (var j = 0; j < formRegister.elements.length; j++) {
            var current = formRegister.elements[j];

            if (errName === current.name) {
              createErrorNode(current, message);
            }
          }
        }

        toggleSubmitBtnDisable();
      }
    });
  }
});
;// CONCATENATED MODULE: ./src/js/send-employee-data.js



document.addEventListener("DOMContentLoaded", function () {
  var employeeForm = document.querySelector(".js-employee-form.show");
  if (!employeeForm) return;
  var phone = employeeForm.querySelector('[type=\"phone\"]'),
      phMask = (0,esm/* default */.ZP)(phone, {
    mask: '+{7}(000)000-00-00'
  }),
      form = employeeForm.querySelector('form'),
      checkboxes = employeeForm.querySelectorAll("input[type=\"checkbox\"]"),
      inputs = employeeForm.querySelectorAll("input:not([type=\"checkbox\"]):not([type=\"submit\"]):not([type=\"reset\"]):not([type=\"button\"])"),
      email = employeeForm.querySelector("input[name=\"email\"]"),
      selects = employeeForm.querySelectorAll("select"),
      btnSubmit = employeeForm.querySelector("input[type=\"submit\"]"),
      violations = 0;
  checkTogglerCheckboxPosition();

  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener("change", toggleCheckboxHelper);
  }

  for (var _i = 0; _i < selects.length; _i++) {
    selects[_i].addEventListener("input", removeSingleErrorNode);
  }

  for (var _i2 = 0; _i2 < inputs.length; _i2++) {
    inputs[_i2].addEventListener("input", removeSingleErrorNode);
  }

  employeeForm.addEventListener("submit", fetchForm);

  function toggleCheckboxHelper() {
    choosePosition(this);
  }

  function checkTogglerCheckboxPosition() {
    for (var _i3 = 0; _i3 < checkboxes.length; _i3++) {
      choosePosition(checkboxes[_i3]);
    }
  }

  function choosePosition(node) {
    var thumb = node.nextElementSibling.querySelector(".js-thumb");

    if (node.checked) {
      thumb.style.left = node.nextElementSibling.getBoundingClientRect().width - thumb.getBoundingClientRect().width + "px";
      node.nextElementSibling.style.backgroundColor = "green";
    } else {
      thumb.style.left = "";
      node.nextElementSibling.style.backgroundColor = "";
    }
  }

  function fetchForm(e) {
    e.preventDefault();
    removeAllErrors();
    validateForm();
    if (violations > 0) return;
    toggleSubmitBtnDisable();
    fetchData();
  }

  function fetchData() {
    var url = "/assets/data/employee-success.json",
        data = new FormData(form);
    var response = fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: data
    }).then(function (response) {
      return response.json();
    }).then(function (result) {
      if (result.err === 0) {
        var modals = document.querySelectorAll(".modal"),
            modalSuccess = document.querySelector(".modal-status");

        for (var _i4 = 0; _i4 < modals.length; _i4++) {
          if (modals[_i4].classList.contains("show")) {
            modals[_i4].classList.remove("show");
          }
        }

        modalSuccess.classList.add("show");
        modalSuccess.querySelector(".modal__title").innerHTML = result.elements[0].message;
        modalSuccess.querySelector("p.modal__info").innerHTML = result.elements[1].message;
        toggleSubmitBtnDisable();
        return;
      } else {
        for (var _i5 = 0; _i5 < result.elements.length; _i5++) {
          var message = result.elements[_i5].message,
              errName = result.elements[_i5].errName;

          for (var j = 0; j < form.elements.length; j++) {
            var current = form.elements[j];

            if (errName === current.name) {
              createErrorNode(current, message);
            }
          }
        }

        toggleSubmitBtnDisable();
      }
    });
  }

  function validateForm() {
    violations = 0;
    validateInputs();
    validateEmail();
    validatePhone();
    validateSelects();
  }

  function validateInputs() {
    for (var _i6 = 0; _i6 < inputs.length; _i6++) {
      if (inputs[_i6].name === "email" || inputs[_i6].name === "phone") continue;

      if (!inputs[_i6].value) {
        createErrorNode(inputs[_i6], "Поле обязательно для заполнения");
        violations++;
      }
    }
  }

  function validatePhone() {
    if (!phone.value) {
      createErrorNode(phone, "Поле обязательно для заполнения");
      violations++;
      return;
    }

    if (phone.value.length !== 16) {
      createErrorNode(phone, "Некорректный номер телефона");
      violations++;
      return;
    }
  }

  function validateSelects() {
    for (var _i7 = 0; _i7 < selects.length; _i7++) {
      if (!selects[_i7].value) {
        createErrorNode(selects[_i7], "Поле обязательно для заполнения");
        violations++;
      }
    }
  }

  function validateEmail() {
    var regExp = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

    if (!email.value) {
      createErrorNode(email, "Поле обязательно для заполнения");
      violations++;
      return;
    }

    if (email.value.match(regExp) === null) {
      createErrorNode(email, "Некорректный email");
      violations++;
      return;
    }
  }

  function toggleSubmitBtnDisable() {
    btnSubmit.disabled = !btnSubmit.disabled;
    var newLabel = btnSubmit.dataset.wait;
    btnSubmit.dataset.wait = btnSubmit.value;
    btnSubmit.value = newLabel;
  }

  function createErrorNode(current, message) {
    var p = document.createElement("p");
    p.classList.add("text-error");
    current.classList.add("border-error");
    p.innerHTML = message;
    current.after(p);
  }

  function removeSingleErrorNode() {
    var isErrorNode = this.classList.contains("border-error");

    if (isErrorNode) {
      this.classList.remove("border-error");
      this.nextElementSibling.remove();
    }
  }

  function removeAllErrors() {
    var inputs = employeeForm.querySelectorAll("input"),
        selects = employeeForm.querySelectorAll("select"),
        p = employeeForm.querySelectorAll(".text-error");
    if (!p.length) return;

    for (var _i8 = 0; _i8 < inputs.length; _i8++) {
      if (inputs[_i8].classList.contains("border-error")) {
        inputs[_i8].classList.remove("border-error");

        inputs[_i8].nextElementSibling.remove();
      }
    }

    for (var _i9 = 0; _i9 < selects.length; _i9++) {
      if (selects[_i9].classList.contains("border-error")) {
        selects[_i9].classList.remove("border-error");

        selects[_i9].nextElementSibling.remove();
      }
    }
  }
});
// EXTERNAL MODULE: ./src/js/lk.js
var lk = __webpack_require__(46);
;// CONCATENATED MODULE: ./src/js/index.js









;// CONCATENATED MODULE: ./src/index.js
// JS
 // SCSS


 // import "swiper/css/swiper.min.css"
// import 'swiper/swiper-bundle.css';

 // Vue.js

/*window.Vue = require('vue')

// Vue components (for use in html)
Vue.component('example-component', require('./components/Example.vue').default)

// Vue init
const app = new Vue({
  el: '#app'
})
 */

/***/ }),

/***/ 604:
/***/ (function() {

"use strict";


document.addEventListener("DOMContentLoaded", function () {
  var accordeon = document.querySelector(".accordeon");
  if (!accordeon) return;
  var btn = accordeon.querySelectorAll(".accordeon__button");

  for (var i = 0; i < btn.length; i++) {
    var curr = btn[i];
    curr.addEventListener("click", function () {
      this.classList.toggle("active");
      var isActive = this.classList.contains("active");

      if (!isActive) {
        this.nextElementSibling.style.maxHeight = null;
      } else {
        this.nextElementSibling.style.maxHeight = this.nextElementSibling.scrollHeight + "px";
      }
    });
  }
});

/***/ }),

/***/ 46:
/***/ (function() {

var ClOSE_TIMER = 15,
    remaining,
    countdownInterval;
/*
* Common functions
*/

document.addEventListener('DOMContentLoaded', function () {
  var closeBtns = document.querySelectorAll('.modal__link--close-popup');

  if (closeBtns.length) {
    [].forEach.call(closeBtns, function (closeBtn) {
      closeBtn.addEventListener('click', function () {
        this.closest('.modal').classList.remove('show');
        clearInterval(countdownInterval);
      });
    });
  }
  /*
  * Show comment
  */


  document.addEventListener('click', function (e) {
    var commentBtn = e.target.closest('.icon-btn--comment');
    var old = document.querySelector('.comment-popup__popup--active');

    if (old) {
      old.classList.remove('comment-popup__popup--active');
    }

    if (commentBtn) {
      commentBtn.parentNode.querySelector('.comment-popup__popup').classList.add('comment-popup__popup--active');
    }
  });
});
/*
* delete order popup
*/

document.addEventListener('DOMContentLoaded', function () {
  var deletePopup = document.querySelector('.modal--delete-order, .modal--delete');

  if (!deletePopup) {
    return;
  }

  var deleteBtns = document.querySelectorAll('.icon-btn--delete'),
      confirmDelete = deletePopup.querySelector('.button--black'),
      totalCountEl = document.querySelector('.pretty-table-footer__count .value');
  var timerEl = deletePopup.querySelector('.seconds');
  document.addEventListener('click', function (e) {
    var btn = e.target.closest('.icon-btn--delete');

    if (!btn) {
      return;
    }

    deletePopup.dataId = btn.dataset.id;

    if (btn.hasAttribute('data-order')) {
      var deletePopupNumber = deletePopup.querySelector('.delete-order__number');
      deletePopupNumber.textContent = '№' + deletePopup.dataId;
    }

    if (btn.hasAttribute('data-employee')) {
      var wrp = btn.closest('tr'),
          employee = wrp.querySelector('.employee').textContent,
          branch = wrp.querySelector('.branch').textContent;
      deletePopup.querySelector('.modal__title--name').textContent = employee;
      deletePopup.querySelector('.modal__title--branch').textContent = branch;
    }

    initCodeResend();
    deletePopup.classList.add('show');
  });
  confirmDelete.addEventListener('click', function () {
    var url = "/assets/data/delete-order.json",
        data = new FormData();
    data.append('id', deletePopup.dataId);
    confirmDelete.disabled = true;
    var response = fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: data
    }).then(function (response) {
      return response.json();
    }).then(function (result) {
      if (result.err === 0) {
        var target = document.querySelector('.icon-btn--delete[data-id="' + deletePopup.dataId + '"]').closest('tr');

        if (target) {
          target.remove();
        }

        confirmDelete.disabled = false;
        deletePopup.classList.remove('show');
        totalCountEl.textContent = result.totalCount;
      }
    });
  });

  function countdown(remaining) {
    timerEl.textContent = remaining;

    if (remaining === 0) {
      deletePopup.classList.remove('show');
      clearInterval(countdownInterval);
      return;
    }
  }

  function initCodeResend() {
    remaining = ClOSE_TIMER;
    timerEl.textContent = remaining;
    countdownInterval = setInterval(function () {
      remaining -= 1;
      countdown(remaining);
    }, 1000);
  }
});
/*
* orders
*/

document.addEventListener('DOMContentLoaded', function () {
  var submitBtn = document.querySelector('.cart-footer__submit');

  if (!submitBtn) {
    return;
  }

  submitBtn.addEventListener('click', function () {
    var url = "/assets/data/order-create.html",
        data = new FormData();
    submitBtn.disabled = true;
    var response = fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: data
    }).then(function (response) {
      return response.text();
    }).then(function (response) {
      document.body.insertAdjacentHTML('beforeEnd', response);
      console.log('document.body: ', document.body);
    });
  }); // clear cart

  var clearBtn = document.querySelector('.cart-footer__link--clear'),
      emptyCartPopup = document.querySelector('.modal--empty-cart'),
      timerEl = emptyCartPopup.querySelector('.seconds');
  clearBtn.addEventListener('click', function () {
    emptyCartPopup.classList.add('show');

    function countdown(remaining) {
      timerEl.textContent = remaining;

      if (remaining === 0) {
        emptyCartPopup.classList.remove('show');
        clearInterval(countdownInterval);
        return;
      }
    }

    remaining = ClOSE_TIMER;
    timerEl.textContent = remaining;
    countdownInterval = setInterval(function () {
      remaining -= 1;
      countdown(remaining);
    }, 1000);
  });
});
/*
* employees
*/

document.addEventListener('DOMContentLoaded', function () {
  var popup = document.querySelector('.modal--employee');

  if (!popup) {
    return;
  }

  var newEmpBtn = document.querySelector('[data-new-employee-btn]'),
      empForm = popup.querySelector('.e-form'),
      titleNew = popup.querySelector('.modal__title--new'),
      titleEdit = popup.querySelector('.modal__title--edit');
  document.addEventListener('click', function (e) {
    var edit = e.target.closest('.icon-btn--edit');

    if (edit) {
      var line = edit.closest('tr'),
          name = line.querySelector('.employee').textContent,
          branch = line.querySelector('.branch').textContent,
          tel = line.querySelector('.tel').textContent,
          email = line.querySelector('.email').textContent,
          price = line.querySelector('.price').checked,
          checkout = line.querySelector('.checkout').checked;
      empForm.querySelector('.e-form__name').value = name;
      empForm.querySelector('.e-form__branch').value = branch;
      empForm.querySelector('.e-form__tel').value = tel;
      empForm.querySelector('.e-form__email').value = email;
      empForm.querySelector('.e-form__price').checked = price;
      empForm.querySelector('.e-form__checkout').checked = checkout;
      titleNew.style.display = 'none';
      titleEdit.style.display = '';
      popup.classList.add('show');
    }
  });
  newEmpBtn.addEventListener('click', function () {
    empForm.reset();
    popup.classList.add('show');
    titleEdit.style.display = 'none';
    titleNew.style.display = '';
  });
});
/*
* Submit form
*/

document.addEventListener('DOMContentLoaded', function () {
  var form = document.querySelector('.c-form');

  if (!form) {
    return;
  }

  var telInput = document.querySelector('.input-line__input--tel');

  if (telInput) {
    IMask(telInput, {
      mask: '+{7}(000)000-00-00'
    });
  }

  var requireElements = form.querySelectorAll('[required]'),
      submitBtn = form.querySelector('.c-form__submit'),
      state = submitBtn.textContent;

  function clearFormErrors() {
    var texts = form.querySelectorAll('.c-form__inline-error:not(.hidden)');
    texts.forEach(function (text) {
      text.classList.add('hidden');
    });
    var styles = form.querySelectorAll('.has-error');
    styles.forEach(function (style) {
      style.classList.remove('has-error');
    });
  }

  function removeSingleErrorNode() {
    var isErrorNode = this.classList.contains('has-error');

    if (isErrorNode) {
      this.classList.remove('has-error');
      this.nextElementSibling.classList.add('hidden');
    }
  }

  function scrollToFirstError() {
    var errEl = form.querySelector('.has-error');

    if (errEl) {
      errEl.closest('.input-line').scrollIntoView({
        behavior: 'smooth'
      });
    }
  }

  requireElements.forEach(function (el) {
    el.addEventListener('input', removeSingleErrorNode);
  });
  form.addEventListener('submit', function (e) {
    e.preventDefault();
    clearFormErrors();
    var bError = false;
    requireElements.forEach(function (el) {
      if (!el.checkValidity()) {
        var errorType = el.validity.valueMissing ? 'valueMissing' : 'valueInvalid';
        var errorEl = el.parentNode.querySelector('.c-form__inline-error');

        if (errorEl) {
          errorEl.innerHTML = el.dataset[errorType];
          errorEl.classList.remove('hidden');
        }

        el.classList.add('has-error');
        bError = true;
      }
    });

    if (bError) {
      scrollToFirstError();
      return;
    }

    var request = new XMLHttpRequest();
    request.responseType = 'json';
    request.open(form.getAttribute('method'), form.getAttribute('action'), true);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
    request.addEventListener("readystatechange", function (e) {
      if (request.readyState === 4 && request.status === 200) {
        if (!this.response.err) {
          var sucessEls = document.querySelectorAll('.success-hide');
          sucessEls.forEach(function (el) {
            el.remove();
          });
          var p = document.createElement('p');
          p.classList.add('c-form__text-success');
          p.textContent = this.response.elements[0].message;
          form.appendChild(p);
        } else {
          this.response.elements.forEach(function (el) {
            var errEl = document.querySelector('[name="' + el.errName + '"]');

            if (errEl) {
              errEl.classList.add('has-error');
              var errText = errEl.nextElementSibling;
              errText.textContent = el.message;
              errText.classList.remove('hidden');
            }
          });
          scrollToFirstError();
          submitBtn.disabled = false;
        }

        submitBtn.textContent = state;
      }
    });
    submitBtn.disabled = true;
    submitBtn.textContent = submitBtn.dataset.wait;
    data = new FormData(form);
    request.send(data);
  });
  /*
  * new pass validation
  */

  var newPassInputs = document.querySelectorAll('.new-pass-input');
  newPassInputs.forEach(function (input) {
    input.addEventListener('change', function () {
      if (newPassInputs[0].value === newPassInputs[1].value) {
        newPassInputs[1].setCustomValidity('');
      } else {
        newPassInputs[1].setCustomValidity(newPassInputs[1].dataset.valueInvalid);
      }
    });
  });
});
/*
* Search panel
*/

document.addEventListener('DOMContentLoaded', function () {
  var searchInput = document.querySelector('.search-line__input');

  if (!searchInput) {
    return;
  }

  var request = new XMLHttpRequest(),
      dataContainer = document.querySelector('.data-container'),
      preloader = document.querySelector('.search-preloader');
  searchInput.addEventListener('input', function () {
    var val = searchInput.value;

    if (typeof callApiRequest != 'undefined' && request.readyState != 4) {
      request.abort();
    }

    request.open('post', searchInput.dataset.action, true);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
    request.addEventListener("readystatechange", function (e) {
      if (this.readyState != 4) return;
      dataContainer.innerHTML = this.response;
      dataContainer.style.display = '';
      preloader.style.display = '';
    });
    dataContainer.style.display = 'none';
    preloader.style.display = 'block';
    var data = 'value=' + searchInput.value;
    request.send(data);
  });
});

/***/ }),

/***/ 927:
/***/ (function() {

"use strict";
 // import Swiper from "swiper";
//scroll to top

document.addEventListener("DOMContentLoaded", function () {
  var toTop = document.querySelector(".btn-to-top"),
      clientHeight = document.documentElement.clientHeight,
      offset = null;
  window.addEventListener("scroll", findYOffset);
  toTop.addEventListener("click", scrollToTop);

  function findYOffset() {
    offset = window.pageYOffset;

    if (offset >= clientHeight) {
      toTop.style.display = "block";
    } else {
      toTop.style.display = "";
    }
  }

  function scrollToTop() {
    var windowTop, timer;
    windowTop = document.body.scrollTop || window.pageYOffset;
    timer = setInterval(function () {
      if (windowTop > 0) window.scroll(0, windowTop -= 45);else clearInterval(timer);
    }, 5);
  }
}); //clearfix menu to filters

document.addEventListener("DOMContentLoaded", function () {
  var block = document.querySelector(".js-height-clearfix"),
      picture = document.querySelector(".js-height-image");
  if (!picture) return;
  window.addEventListener("resize", calcBlockHeight);
  calcBlockHeight();

  function calcBlockHeight() {
    var height = picture.getBoundingClientRect().height,
        padding = 15;

    if (screen.width > 991) {
      block.style.height = height + padding + "px";
    } else block.style.height = 0 + "px";
  }
}); //toggle card item width

document.addEventListener("DOMContentLoaded", function (e) {
  var btnSize = document.querySelector(".filters__menu");
  var blocks = document.querySelectorAll(".sep");
  var breakpoint = 767;
  if (!btnSize || !blocks) return;
  btnSize.addEventListener("click", toggleCardSize);
  window.addEventListener("resize", function () {
    for (var i = 0; i < blocks.length; i++) {
      var current = blocks[i];

      if (screen.width > breakpoint) {
        current.classList.remove("col-6");
        current.classList.add("col-sm-12");
        current.querySelector(".card__price-block").style.fontSize = "";
        current.querySelector(".card__model").style.fontSize = "";
        current.querySelector(".card__brand").style.fontSize = "";
      }
    }
  });

  function toggleCardSize(e) {
    for (var i = 0; i < blocks.length; i++) {
      var current = blocks[i];

      if (current.classList.contains("col-sm-12")) {
        current.classList.remove("col-sm-12");
        current.classList.add("col-6");
        current.querySelector(".card__price-block").style.fontSize = 1 + "rem";
        current.querySelector(".card__model").style.fontSize = 1 + "rem";
        current.querySelector(".card__brand").style.fontSize = 1.5 + "rem";
      } else {
        current.classList.remove("col-6");
        current.classList.add("col-sm-12");
        current.querySelector(".card__price-block").style.fontSize = "";
        current.querySelector(".card__model").style.fontSize = "";
        current.querySelector(".card__brand").style.fontSize = "";
      }
    }
  }
}); //display filters

document.addEventListener("DOMContentLoaded", function () {
  var btnBox = document.querySelector(".js-box-filter"),
      filterBox = document.querySelector(".filters__sets");
  if (!btnBox) return;
  var filter = btnBox.querySelector(".filters__btn[data-filter=\"filter\"]").dataset.filter;
  var search = btnBox.querySelector(".filters__btn[data-filter=\"search\"]").dataset.filter;
  btnBox.addEventListener("click", toggleFilter);
  window.addEventListener("resize", function () {
    filterBox.querySelector('div[data-target=\"' + filter + '\"]').style.maxHeight = "";
    filterBox.querySelector('div[data-target=\"' + filter + '\"]').classList.remove("show");
    filterBox.querySelector('div[data-target=\"' + search + '\"]').style.maxHeight = "";
    filterBox.querySelector('div[data-target=\"' + search + '\"]').classList.remove("show");
  });

  function toggleFilter(e) {
    var target = e.target;
    if (!target.closest(".filters__btn") || target.closest(".filters__menu")) return;
    var attr = target.dataset.filter,
        filter = filterBox.querySelector('div[data-target=\"' + attr + '\"]');

    if (filter.classList.contains('show')) {
      filter.classList.remove('show');
      filter.style.maxHeight = "";
      return;
    }

    filter.classList.add('show');
    filter.style.maxHeight = filter.scrollHeight + "px";
  }
}); // item card

document.addEventListener("DOMContentLoaded", function () {
  var itemForm = document.getElementById('order');

  if (!itemForm) {
    return;
  }

  itemForm.addEventListener('keypress', function (e) {
    if (e.key === "Enter") {
      e.preventDefault();
      return false;
    }
  });
  var orderBtn = document.querySelector('.btn-order'),
      cartPreview = document.querySelector('.order-number');
  orderBtn.addEventListener('click', function () {
    cartPreview.classList.add('shaker');
    setTimeout(function () {
      cartPreview.classList.remove('shaker');
    }, 820);
  });
}); //counter items
// document.addEventListener("DOMContentLoaded", function () {
//     let counter = document.querySelector(".js-count-items");
//     if (!counter) return;
//     let btns = counter.querySelectorAll('.js-count-items button'),
//         inputs = counter.querySelectorAll('.js-count-items input');
//     btns.forEach(function(btn){
//         btn.addEventListener('click', function(){
// 			var valueEl = this.closest('.js-count-items').querySelector('input');
// 			if (this.classList.contains('js-count-decrease')){
// 				if (valueEl.value != valueEl.getAttribute('min')){
// 					valueEl.value = parseInt(valueEl.value) - 1;
// 				}
// 			} else {
// 				if (valueEl.value != valueEl.getAttribute('max')){
// 					valueEl.value = parseInt(valueEl.value) + 1;
// 				}
// 			}
// 			var event = new Event('change');
// 			valueEl.dispatchEvent(event);
// 		});
//     });
//     inputs.forEach(function(input){
//         input.addEventListener('keyup', function(){
//             var _input = this;
//             if (!_input.value){
//                 _input.value = _input.getAttribute('min');
//             } else {
//                 if ( parseInt(_input.value) < parseInt(_input.getAttribute('min')) ){
//                     _input.value = _input.getAttribute('min');
//                 }
//                 if ( parseInt(_input.value) > parseInt(_input.getAttribute('max')) ){
//                     _input.value = _input.getAttribute('max');
//                 }
//             }
//         });
//     });
// });
//toggle sm-form (cooperation.html)

document.addEventListener("DOMContentLoaded", function () {
  var breakpoint = 991,
      form = document.querySelector(".form-block__form"),
      btn = document.querySelector(".js-form-toggler");
  if (!form || !btn) return;

  window.onresize = function () {
    if (document.documentElement.clientWidth > breakpoint) {
      btn.classList.remove("active");
      findFormVisibility();
    }
  };

  btn.addEventListener("click", toggleForm);
  findFormVisibility();

  function findFormVisibility() {
    if (document.body.clientWidth > breakpoint) {
      form.style.maxHeight = form.scrollHeight + "px";
    } else {
      form.style.maxHeight = "";
    }
  }

  function toggleForm() {
    if (document.body.clientWidth <= breakpoint) {
      if (!this.nextElementSibling.style.maxHeight) {
        this.classList.add("active");
        this.nextElementSibling.style.maxHeight = this.nextElementSibling.scrollHeight + "px";
        return;
      }

      this.classList.remove("active");
      this.nextElementSibling.style.maxHeight = "";
    }
  }
}); //Swiper item + change vendor & sliders functionality

document.addEventListener("DOMContentLoaded", function () {
  var colorsInputs = document.querySelectorAll(".js-color"),
      vendors = document.querySelectorAll(".specific__vendor"),
      sizes = document.querySelectorAll(".js-size"),
      swItem = null;
  if (!colorsInputs.length || !vendors.length || !sizes.length) return;
  activateAll();

  for (var i = 0; i < document.querySelectorAll(".js-color-label").length; i++) {
    var current = document.querySelectorAll(".js-color-label")[i];
    current.addEventListener("click", activateAll);
  }

  for (var _i = 0; _i < document.querySelectorAll(".js-size-label").length; _i++) {
    var _current = document.querySelectorAll(".js-size-label")[_i];

    _current.addEventListener("click", activateAll);
  }

  function getActiveAttrs() {
    var attrs = [];

    for (var _i2 = 0; _i2 < colorsInputs.length; _i2++) {
      var _current2 = colorsInputs[_i2];

      if (_current2.checked) {
        var color = _current2.nextElementSibling.dataset.color;
        attrs.push(color);
        break;
      }
    }

    for (var _i3 = 0; _i3 < sizes.length; _i3++) {
      var _current3 = sizes[_i3];

      if (_current3.checked) {
        var size = _current3.nextElementSibling.dataset.size;
        attrs.push(size);
        break;
      }
    }

    return attrs;
  }

  function activateAll() {
    deactivateAll();
    var attrs = getActiveAttrs(); //slider

    var slider = document.querySelector('.js-sw-change[data-slider=\"' + attrs[0] + '\"]');
    slider.classList.add("active");
    swItem = new Swiper('.js-sw-change[data-slider=\"' + attrs[0] + '\"]', {
      direction: 'horizontal',
      autoHeight: true,
      freeMode: false,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      }
    }); //Vendor

    var vendorsByColor = document.querySelectorAll('.specific__vendor--color[data-target-color=\"' + attrs[0] + '\"]');

    for (var _i4 = 0; _i4 < vendorsByColor.length; _i4++) {
      var vendor = vendorsByColor[_i4].querySelector('.specific__vendor--size[data-target-size=\"' + attrs[1] + '\"]');

      vendor.classList.add("active");
    }
  }

  function deactivateAll() {
    var sliders = document.querySelectorAll('.js-sw-change');

    for (var _i5 = 0; _i5 < sliders.length; _i5++) {
      sliders[_i5].classList.remove("active");
    }

    swItem = null;
    var vendors = document.querySelectorAll(".specific__vendor--size");

    for (var _i6 = 0; _i6 < vendors.length; _i6++) {
      vendors[_i6].classList.remove("active");
    }
  }
}); //animation on main page

document.addEventListener("DOMContentLoaded", function () {
  var link = document.querySelector(".js-image"),
      width = document.documentElement.clientWidth;
  if (!link) return;
  window.addEventListener("resize", calculateNewLeftCoord);
  window.addEventListener("scroll", preventScroll);
  link.style.opacity = 1;
  link.style.width = 165 + "px";
  link.style.top = 8.1 + "rem";
  link.style.left = width / 2 - 83 + "px";
  link.addEventListener("transitionend", function () {
    link.parentNode.remove();
    window.removeEventListener("scroll", preventScroll);
  });

  function preventScroll(e) {
    if (document.querySelector(".header-js-animation")) {
      this.scroll(0, 0);
      e.preventDefault();
    }
  }

  function calculateNewLeftCoord() {
    width = document.documentElement.clientWidth;
    link.style.left = width / 2 - 83 + "px";
  }
}); //Toggle sidebar sub-menu

document.addEventListener("DOMContentLoaded", function () {
  var sidebar = document.querySelector(".sidebar"),
      item;
  if (!sidebar) return; // sidebar.addEventListener("click", toggleSubMenu);
  // function toggleSubMenu(e) {
  //     let target = e.target;
  //     if (target.closest(".sidebar__link")) {
  //         if (target.parentNode.querySelector(".sub-catalog")) {
  //             e.preventDefault();
  //             deactiveteAllElements(target);
  //             target.parentNode.classList.toggle("active");
  //             item = target.parentNode;
  //         }
  //     }
  // }

  function deactiveteAllElements(target) {
    var allItems = sidebar.querySelectorAll(".sidebar__link");

    for (var i = 0; i < allItems.length; i++) {
      if (target.parentNode === item) {
        continue;
      }

      allItems[i].parentNode.classList.remove("active");
    }
  }
}); //Add sticky top menu

document.addEventListener("DOMContentLoaded", function () {
  var header = document.querySelector("header.header:not(.animate__animated)");

  if (!header) {
    return;
  }

  var headerHeight = header.offsetHeight,
      navHeight = 0,
      scrollPos;
  window.addEventListener("scroll", addStickyMenu);

  function addStickyMenu() {
    var coordY = window.pageYOffset;

    if (header.classList.contains("sticky")) {
      navHeight = document.querySelector(".sticky").getBoundingClientRect().height;
    }

    if (scrollPos < coordY) {
      if (scrollPos > headerHeight + navHeight) {
        header.classList.add("sticky");
        addAnimation("animate__animated", "animate__slideInDown");
      }
    }

    if (scrollPos > coordY) {
      if (coordY <= headerHeight - navHeight && header.classList.contains("sticky")) {
        removeAnimation("animate__animated", "animate__slideInDown");
        header.classList.remove("sticky");
      }
    }

    scrollPos = coordY;
  }

  function removeAnimation(animation, animationStyle) {
    if (header.classList.contains(animationStyle) && header.classList.contains(animation)) {
      header.classList.remove(animation);
      header.classList.remove(animationStyle);
    }
  }

  function addAnimation(animation, animationStyle) {
    if (!header.classList.contains(animationStyle) && !header.classList.contains(animation)) {
      header.classList.add(animation);
      header.classList.add(animationStyle);
    }
  }
}); //Link in link on catalog page

document.addEventListener("DOMContentLoaded", function () {
  var catalog = document.querySelectorAll(".catalog");
  if (!catalog.length) return;

  for (var i = 0; i < catalog.length; i++) {
    catalog[i].addEventListener("click", openCatalog);
  }

  function openCatalog(e) {
    if (e.target.tagName !== "A") {
      window.location.href = this.dataset.url;
    }
  }
}); // change sw-menu bg on hover
// document.addEventListener('DOMContentLoaded', function(){
//     var links = document.querySelectorAll('.sw-menu__image-wrapper .brands__link');
//     if (!links.length) {
//         return;
//     }
//     links.forEach(function(link){
//         link.addEventListener('mouseover', function(){
//             let wrp = this.closest('.sw-menu__image-wrapper');
//             wrp.style.backgroundImage = 'url("'+this.dataset.img+'")';
//         });
//     })
// })

/*
* Cookie warning 
*/

function setCookie(name, value, options) {
  options = options || {};
  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }

  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);
  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];

    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

document.addEventListener('DOMContentLoaded', function () {
  var btn = document.querySelector('.cow__close');

  if (btn) {
    var win = document.querySelector('.cow'),
        isShowed = getCookie('cookieShowed');

    if (!isShowed) {
      win.style.display = 'flex';
      btn.addEventListener('click', function () {
        win.style.display = 'none';
        setCookie('cookieShowed', 1, {
          expires: 60 * 60 * 24 * 14
        });
      });
    }
  }
});

/***/ }),

/***/ 722:
/***/ (function() {

"use strict";


document.addEventListener("DOMContentLoaded", function () {
  var menu = document.querySelector(".header .nav");

  if (!menu) {
    return;
  }

  var items = menu.querySelectorAll(".nav__item"),
      burger = document.querySelector("#toggle"),
      breakpoint = 991,
      curr = null;
  chooseListeners();

  function chooseListeners() {
    menu.addEventListener("click", toggleSmSubMenu);

    if (document.documentElement.clientWidth > breakpoint) {
      menu.addEventListener("mouseover", findLgSubMenu);
      menu.addEventListener("mouseout", removeLgSubMenu);
    } else {
      burger.addEventListener("click", toggleSmMenu);
    }
  }

  function removeAllListeners() {
    menu.removeEventListener("mouseover", findLgSubMenu);
    menu.removeEventListener("mouseout", removeLgSubMenu);

    for (var i = 0; i < items.length; i++) {
      items[i].removeEventListener("click", toggleSmSubMenu);
    }
  }

  window.addEventListener("resize", function (e) {
    if (document.documentElement.clientWidth > breakpoint) {
      burger.classList.remove("cross");
      removeAllSmSubMenu();
    }

    removeAllListeners();
    chooseListeners();
  }); //sm-menu functions

  function toggleSmMenu() {
    toggleBurgerIcon();
    removeAllSmSubMenu();
    menu.classList.toggle("show");
  }

  function toggleBurgerIcon() {
    burger.classList.toggle("cross");
  }

  function toggleSmSubMenu(e) {
    var target = e.target.closest(".nav__item"),
        subMenu = target.querySelector(".sub-menu");
    if (!subMenu) return;

    if (!subMenu.style.maxHeight) {
      subMenu.style.maxHeight = subMenu.scrollHeight + "px";
      subMenu.parentNode.classList.add("active");

      window.onscroll = function () {
        return false;
      };
    } else {
      subMenu.style.maxHeight = "";
      subMenu.parentNode.classList.remove("active");
    }
  }

  function removeAllSmSubMenu() {
    var subMenu = document.querySelectorAll(".sub-menu");

    for (var i = 0; i < subMenu.length; i++) {
      subMenu[i].parentNode.classList.remove("active");
      subMenu[i].style.maxHeight = "";
    }
  } //lg-menu functions


  function findLgSubMenu(e) {
    if (curr && curr != this) return;
    var target = e.target.closest('.nav__item');
    if (!target) return;
    var subMenu = target.querySelector(".sub-menu");

    if (curr == this) {
      removeLgSubMenu(e, target);
      curr = target;

      if (!subMenu) {
        removeLgSubMenu(e, target);
        return;
      }

      showLgSubMenu(target);
      return;
    }

    if (!subMenu) return;
    curr = target;
    showLgSubMenu(target);
  }

  function removeLgSubMenu(e, node) {
    if (!curr) return;
    var related = e.relatedTarget;

    while (related) {
      if (related == node) {
        curr = node;
        return;
      }

      related = related.parentNode;
    }

    removeAllSmSubMenu();
    curr = null;
  }

  function showLgSubMenu(node) {
    node.classList.add("active");
    node.querySelector(".sub-menu").style.maxHeight = node.querySelector(".sub-menu").scrollHeight + "px";
  }
});

/***/ }),

/***/ 28:
/***/ (function() {

// import Swiper from 'swiper';
//First slider
document.addEventListener("DOMContentLoaded", function () {
  var slider = document.querySelector(".swiper-main");
  if (!slider) return;
  var slidesCount = slider.querySelector('.js-swiper-count'),
      currentSlide = slider.querySelector('.js-slide-count');
  var swiperMain = new Swiper('.swiper-main', {
    autoHeight: true,
    loop: true,
    autoplay: {
      delay: 9000
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    on: {
      init: function init() {
        slidesCount.textContent = this.slides.length - 2;
      },
      slideChange: function slideChange() {
        currentSlide.textContent = this.realIndex + 1;
      }
    }
  });
}); //Swiper - menu

document.addEventListener("DOMContentLoaded", function () {
  var items = document.querySelectorAll(".sw-menu__item"),
      allSliders = document.querySelectorAll(".sw-menu__slider"),
      allImages = document.querySelectorAll(".sw-menu__image-wrapper");
  var swMenu = null;
  if (!items.length || !allSliders.length || !allImages.length) return;
  showActiveItems();

  for (var i = 0; i < items.length; i++) {
    items[i].addEventListener("click", activateLink);
  }

  function activateLink() {
    deactivateAll();
    this.classList.add("active");
    getActiveAttr();
    showActiveItems();
  }

  function deactivateAll() {
    swMenu = null;

    for (var _i = 0; _i < items.length; _i++) {
      items[_i].classList.remove("active");
    }

    for (var _i2 = 0; _i2 < allImages.length; _i2++) {
      allImages[_i2].classList.remove("active");
    }

    for (var _i3 = 0; _i3 < allSliders.length; _i3++) {
      allSliders[_i3].classList.remove("active");
    }
  }

  function getActiveAttr() {
    var attr = "";

    for (var _i4 = 0; _i4 < items.length; _i4++) {
      var current = items[_i4];

      if (current.classList.contains("active")) {
        attr = current.dataset.item;
      }
    }

    return attr;
  }

  function showActiveItems() {
    var activeAttr = getActiveAttr();
    var imageBlock = document.querySelector('.sw-menu__image-wrapper[data-target=\"' + activeAttr + '\"]');
    var slider = document.querySelector('.sw-menu__slider[data-slider=\"' + activeAttr + '\"]');
    slider.classList.add("active");
    imageBlock.classList.add("active");
    swMenu = new Swiper('.sw-menu__slider[data-slider=\"' + activeAttr + '\"]', {
      direction: 'horizontal',
      freeMode: false,
      autoplay: {
        delay: 9000
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        450: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 20
        },
        991: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      },
      slidesPerView: 1,
      spaceBetween: 0
    });
  }
}); //Swiper - news

var newsEl = document.querySelector(".news__slider");

if (newsEl) {
  var swiperNews = new Swiper(newsEl, {
    direction: 'horizontal',
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    scrollbar: {
      el: '.swiper-scrollbar',
      draggable: true
    },
    breakpoints: {
      500: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      991: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      1199: {
        slidesPerView: 4,
        spaceBetween: 20
      },
      1800: {
        slidesPerView: 5,
        spaceBetween: 20
      }
    },
    slidesPerView: 1,
    spaceBetween: 30
  });
} // news on main


document.addEventListener("DOMContentLoaded", function () {
  var slider = document.querySelector(".news-slider");
  if (!slider) return;
  var swiperNews = new Swiper(slider, {
    autoplay: {
      delay: 15000
    },
    navigation: {
      nextEl: slider.querySelector('.slider-btn--next'),
      prevEl: slider.querySelector('.slider-btn--prev')
    }
  });
});
var itemSwiper;

function itemSwiperUpdate() {
  itemSwiper.update();
}

document.addEventListener("DOMContentLoaded", function () {
  var itemSlider = document.querySelector('.swiper-item:not(.js-sw-change), .swiper-item-twin:not(.js-sw-change)');

  if (!itemSlider) {
    return;
  }

  itemSwiper = new Swiper(itemSlider, {
    navigation: {
      nextEl: itemSlider.querySelector('.swiper-button-next'),
      prevEl: itemSlider.querySelector('.swiper-button-prev')
    }
  });
});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			143: 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkstailer_group"] = self["webpackChunkstailer_group"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, [216], function() { return __webpack_require__(968); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;